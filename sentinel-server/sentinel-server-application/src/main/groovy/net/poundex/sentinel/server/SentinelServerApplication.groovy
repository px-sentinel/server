package net.poundex.sentinel.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SentinelServerApplication {

	static void main(String[] args) {
		SpringApplication.run(SentinelServerApplication, args)
	}

}
