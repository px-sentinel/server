package net.poundex.sentinel.server.caretaker.util

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.dao.*
import net.poundex.sentinel.server.caretaker.domain.*
import net.poundex.sentinel.server.caretaker.domain.action.Action
import net.poundex.sentinel.server.caretaker.domain.action.ExecuteMonitorSceneAction
import net.poundex.sentinel.server.caretaker.domain.action.MacroAction
import net.poundex.sentinel.server.caretaker.domain.action.SetDeviceValueAction
import net.poundex.sentinel.server.caretaker.domain.action.SetMonitorValueAction
import net.poundex.sentinel.server.caretaker.domain.action.SetSceneAction
import net.poundex.sentinel.server.caretaker.domain.appliance.Light
import net.poundex.sentinel.server.caretaker.domain.monitor.*
import net.poundex.sentinel.server.caretaker.domain.monitor.value.BooleanMonitorValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.ButtonPushMonitorValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.EnumMonitorValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.SceneMonitorValue
import net.poundex.sentinel.server.caretaker.enviornment.Daypart
import net.poundex.sentinel.server.caretaker.hue.dao.HueBridgeRepository
import net.poundex.sentinel.server.caretaker.hue.domain.HueBridge
import net.poundex.sentinel.server.caretaker.nest.dao.NestThermostatRepository
import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat
import net.poundex.sentinel.server.caretaker.zwave.dao.ZWaveModemRepository
import net.poundex.sentinel.server.caretaker.zwave.domain.ZWaveModem
import net.poundex.sentinel.server.util.StupidSecretsProvider
import org.springframework.boot.CommandLineRunner
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Component

@Component
@Slf4j
class TestData implements CommandLineRunner
{
	private final ZoneRepository zoneRepository
	private final HueBridgeRepository hueBridgeRepository
	private final DeviceManager deviceManager
	private final NestThermostatRepository nestThermostatRepository
	private final ValueReaderRepository valueReaderRepository
	private final MonitorRepository monitorRepository
	private final MonitorValueRepository monitorValueRepository
	private final ActionRepository actionRepository
	private final EnvironmentPredicateRepository environmentPredicateRepository
	private final MonitorHandlerRepository monitorHandlerRepository
	private final MonitorSubscriberRepository monitorSubscriberRepository
	private final LightRepository lightRepository
	private final ZWaveModemRepository zWaveModemRepository

//	private final MessageChannel deviceValueChannel
	private final MessageChannel monitorValueChannel

	private final ActionContext actionContext

	private Zone livingRoom
	private Zone house
	private Zone kitchen

	private Monitor lrTempMonitor

	private Light slamp
	private HueBridge hueBridge
	private ZWaveModem zwaveModem

	private SetDeviceValueAction turnSouthLampOff
	private SetDeviceValueAction turnSouthLampOn
	private MonitorValue bOn
	private MonitorValue bOff

	private Light passageLight
	private Light landingLight
	Action turnPassageLightOn
	Action turnPassageLightOff
	Action turnLandingLightOn
	Action turnLandingLightOff
	private ValueReader passageButtonsReader
	private ButtonPushMonitor passageButtonPushMonitor
	Light nlamp
	Light nceil
	Light sceil
	private Action setPassageAndLandingToScene
	private Monitor passageLandingAutoMonitor
	private SetSceneAction eveningPassageLandingScene
	private Zone bedroom
	private Monitor bedroomAutoMonitor
	private SetMonitorValueAction setPassageLandingToAuto
	private SetMonitorValueAction setPassageLandingToNoAuto
	SetMonitorValueAction setBedroomToAuto
	SetMonitorValueAction setBedroomToNoAuto
	private Light bedroomCeiling
	private Light bedroomThrowLamp
	ValueReader bedroomButtonsReader
	ButtonPushMonitor bedroomButtonPushMonitor
	private Monitor passageLandingSceneMonitor
	private SetSceneAction dayPassageLandingScene
	private SceneMonitorValue dayPassageLandingSceneValue
	private Action turnOffNorthCeil
	private Action turnOffSouthCeil
	private Action turnOffNorthLamp
	private Action turnOffSouthLamp
	private Action turnOffTvLeft
	private Action turnOffTvRight
	private Action turnOffWindow
	private Monitor daylightMonitor
	private Light readingLight
	private Action turnOffBedroomCeil
	private Action turnOffBedroomLamp
	private Action turnOffReadingLight
	BooleanMonitor livingRoomAutoMonitor
	SetMonitorValueAction setLivingRoomToAuto
	SetMonitorValueAction setLivingRoomToNoAuto
	private Monitor bedroomSceneMonitor
	private SceneMonitorValue bedroomEveningSceneMonVal
	private SetSceneAction bedroomW2B
	private SetMonitorValueAction setBedroomSceneToMorning
	private SetSceneAction bedroomMorningScene
	private Action bedroomLightsOn
	private Action bedroomLightsOff
	private Action landingAndPassageOn
	private Action landingAndPassageOff
	private Monitor livingRoomSceneMonitor
	private SetSceneAction lrMorningScene
	private SceneMonitorValue lrDaySceneValue
	SceneMonitorValue lrMorningSceneVal
	private NestThermostat nestThermostat
	SetMonitorValueAction setBedroomSceneToDay
	private Light tvLeft
	private Light tvRight
	private Light window

	TestData(ZoneRepository zoneRepository, HueBridgeRepository hueBridgeRepository, DeviceManager deviceManager, NestThermostatRepository nestThermostatRepository, ValueReaderRepository valueReaderRepository, MonitorRepository monitorRepository, MonitorValueRepository monitorValueRepository, ActionRepository actionRepository, EnvironmentPredicateRepository environmentPredicateRepository, MonitorHandlerRepository monitorHandlerRepository, MonitorSubscriberRepository monitorSubscriberRepository, MessageChannel monitorValueChannel, LightRepository lightRepository, ZWaveModemRepository zWaveModemRepository, ActionContext actionContext)
	{
		this.zoneRepository = zoneRepository
		this.hueBridgeRepository = hueBridgeRepository
		this.deviceManager = deviceManager
		this.nestThermostatRepository = nestThermostatRepository
		this.valueReaderRepository = valueReaderRepository
		this.monitorRepository = monitorRepository
		this.monitorValueRepository = monitorValueRepository
		this.actionRepository = actionRepository
		this.environmentPredicateRepository = environmentPredicateRepository
		this.monitorHandlerRepository = monitorHandlerRepository
		this.monitorSubscriberRepository = monitorSubscriberRepository
//		this.deviceValueChannel = deviceValueChannel
		this.monitorValueChannel = monitorValueChannel
		this.lightRepository = lightRepository
		this.zWaveModemRepository = zWaveModemRepository
		this.actionContext = actionContext
	}

	@Override
	void run(String... args) throws Exception
	{
		basic1()

		installZones()
		installHue()
		installNest()
		zwave()

		bulbs()
		bedroomScenes()
		livingRoomScenes()

		humidityAndTempReadersAndMonitors()
		daylightMonitor()
		stuff2()

		autoMonitors()

		buttons()
		bedroomMotion()
		eveningButton()
		motion1()

		doneDownstairs()
		bed1()
		bed2()
		morning()
		day()

		nightlights()
		readingLight()

		deviceManager.reset()
	}

	private void basic1()
	{
		bOn = monitorValueRepository.save(BooleanMonitorValue.of("On", true))
		bOff = monitorValueRepository.save(BooleanMonitorValue.of("Off", false))
	}

	private void installZones() {
		house = new Zone(name: "House")
		zoneRepository.save this.house

		Zone downstairs = new Zone(name: "Downstairs", parent: this.house)
		zoneRepository.save downstairs

		Zone passage = new Zone(name: "Passage", parent: downstairs)
		zoneRepository.save passage

		Zone livingArea = new Zone(name: "Living Area", parent: downstairs)
		zoneRepository.save livingArea

		Zone lrd = new Zone(name: "Living Room & Dining Room", parent: livingArea)
		zoneRepository.save lrd

		livingRoom = new Zone(name: "Living Room", parent: lrd)
		zoneRepository.save this.livingRoom

		Zone diningRoom = new Zone(name: "Dining Room", parent: lrd)
		zoneRepository.save diningRoom

		kitchen = new Zone(name: "Kitchen", parent: livingArea)
		zoneRepository.save this.kitchen

		Zone upstairs = new Zone(name: "Upstairs", parent: this.house)
		zoneRepository.save upstairs

		bedroom = new Zone(name: "Bedroom", parent: upstairs)
		zoneRepository.save(this.bedroom)

	}

	private void installHue()
	{
		hueBridge = new HueBridge(name: "hue0", address: "192.168.0.22", username: StupidSecretsProvider.instance.secrets.hue.bridgeUser)
		hueBridgeRepository.save this.hueBridge
	}

	private void installNest()
	{
		nestThermostat = new NestThermostat(
				name: "nest0",
				nestDeviceId: StupidSecretsProvider.instance.secrets.nest.deviceId,
				nestAccessToken: StupidSecretsProvider.instance.secrets.nest.accessToken)
		nestThermostatRepository.save this.nestThermostat
	}

	private void humidityAndTempReadersAndMonitors() {
		ValueReader humidReader = valueReaderRepository.save(
				new ValueReader(name: "Nest Humidity Reader", portId: "nest://${nestThermostat.id}/SZ/HUMIDITY"))
		Monitor lrHumidMonitor = monitorRepository.save(new QuantityMonitor(
				name: "Living Room Humidity", sources: [humidReader], location: livingRoom
		))

		ValueReader tempReader = valueReaderRepository.save(
				new ValueReader(name: "Nest Temperature Reader", portId: "nest://${nestThermostat.id}/SZ/ROOM_TEMP_C"))

		lrTempMonitor = monitorRepository.save(new QuantityMonitor(
				name: "Living Room Temperature", sources: [tempReader], location: livingRoom
		))
	}

	private void daylightMonitor()
	{
		ValueReader daylightReader = new ValueReader(name: "Daylight reader",
				portId: "dummy://daylight/IS_DAYLIGHT")
		valueReaderRepository.save daylightReader
		daylightMonitor = new BooleanMonitor(name: "Daylight", sources: [daylightReader], location: house, defaultValue: bOff)
		monitorRepository.save this.daylightMonitor
	}
	
	
	private void stuff2()
	{
		MonitorValue evening = monitorValueRepository.save(EnumMonitorValue.of("Evening", "Daypart", "Evening"))

		Monitor daypart = new EnumMonitor(name: "Daypart", location: house, enumClass: Daypart)
		Monitor occHuman = new BooleanMonitor(name: "Human Occ", location: house)

		monitorRepository.saveAll([daypart, occHuman])

		EnvironmentPredicate daypartIsEvening =
				environmentPredicateRepository.save new MonitorValuePredicate(name: "Daypart is Evening", monitor: daypart, monitorValue: evening)

		EnvironmentPredicate humansAreOccupying =
				environmentPredicateRepository.save new MonitorValuePredicate(name: "Humans are home", monitor: occHuman, monitorValue: this.bOn)

		EnvironmentPredicate eveningAndHumans =
				environmentPredicateRepository.save AndPredicate.of(humansAreOccupying, daypartIsEvening).tap {
					it.name = "humans + evening"
				}
	}

	private void bulbs()
	{
		slamp = lightRepository.save new Light(name: "slamp",
				deviceId: hueBridge.getHardwareId().resolve("bulb/2/self").toString(), location: livingRoom)

		turnSouthLampOn =  actionRepository.save new SetDeviceValueAction(name: "lamp on", appliance: slamp,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "true")

		turnSouthLampOff = actionRepository.save new SetDeviceValueAction(name: "lamp off", appliance: slamp,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")

		//pass 9 land 8

		passageLight = lightRepository.save new Light(name: "Passage", deviceId: hueBridge.getHardwareId().resolve("bulb/9/self").toString(), location: house)
		landingLight = lightRepository.save new Light(name: "Passage", deviceId: hueBridge.getHardwareId().resolve("bulb/8/self").toString(), location: house)



		turnPassageLightOn =  actionRepository.save new SetDeviceValueAction(name: "Passage Light On", appliance: passageLight,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "true")

		turnPassageLightOff = actionRepository.save new SetDeviceValueAction(name: "Passage Light Off", appliance: passageLight,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")

		turnLandingLightOn =  actionRepository.save new SetDeviceValueAction(name: "Landing Light On", appliance: landingLight,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "true")

		turnLandingLightOff = actionRepository.save new SetDeviceValueAction(name: "Landing Light Off", appliance: landingLight,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")




		nlamp = lightRepository.save new Light(name: "nlamp",
				deviceId: hueBridge.getHardwareId().resolve("bulb/1/self").toString(), location: livingRoom)

		nceil = lightRepository.save new Light(name: "nceil",
				deviceId: hueBridge.getHardwareId().resolve("bulb/5/self").toString(), location: livingRoom)

		sceil = lightRepository.save new Light(name: "sceil",
				deviceId: hueBridge.getHardwareId().resolve("bulb/6/self").toString(), location: livingRoom)


		readingLight = new Light(name: "Reading Lamp", location: bedroom, deviceId: hueBridge.hardwareId.resolve("bulb/10/self").toString())
		lightRepository.save this.readingLight

		bedroomCeiling = new Light(name: "Bedroom Ceiling", deviceId: hueBridge.getHardwareId().resolve("bulb/7/self").toString(), location: bedroom)
		lightRepository.save this.bedroomCeiling

		bedroomThrowLamp = new Light(name: "Bedroom Throw Lamp", deviceId: hueBridge.getHardwareId().resolve("bulb/3/self").toString(), location: bedroom)
		lightRepository.save this.bedroomThrowLamp

		tvLeft = new Light(name: "TV Left", deviceId: hueBridge.getHardwareId().resolve("bulb/12/self").toString(), location: livingRoom)
		lightRepository.save this.tvLeft

		tvRight = new Light(name: "TV Right", deviceId: hueBridge.getHardwareId().resolve("bulb/13/self").toString(), location: livingRoom)
		lightRepository.save this.tvRight

		window = new Light(name: "Window", deviceId: hueBridge.getHardwareId().resolve("bulb/14/self").toString(), location: livingRoom)
		lightRepository.save this.window
	}

	private void zwave()
	{
		zwaveModem = zWaveModemRepository.save(new ZWaveModem(name: "zwave0", modemDevice: "/dev/ttyACM0"))
	}

	private void buttons()
	{
		passageButtonsReader = new ValueReader(name: "Passage Buttons Reader", portId: "zwave://${zwaveModem.id}/node/3/SCENE_COMMAND")
		valueReaderRepository.save(this.passageButtonsReader)

		bedroomButtonsReader = new ValueReader(name: "Bedroom Buttons Reader", portId: "zwave://${zwaveModem.id}/node/8/SCENE_COMMAND")
		valueReaderRepository.save(this.bedroomButtonsReader)

		passageButtonPushMonitor = new ButtonPushMonitor(name: "Buttons1", sources: [this.passageButtonsReader], location: house)
		monitorRepository.save(this.passageButtonPushMonitor)

		bedroomButtonPushMonitor = new ButtonPushMonitor(name: "Buttons2", sources: [this.bedroomButtonsReader], location: bedroom)
		monitorRepository.save(this.bedroomButtonPushMonitor)

		MonitorHandler button1Handler = new MonitorHandler(name: "Buttons Handler 1", action: turnSouthLampOn)
		MonitorHandler button2Handler = new MonitorHandler(name: "Buttons Handler 2", action: turnSouthLampOff)
		monitorHandlerRepository.saveAll([button1Handler, button2Handler])

		ButtonPushMonitorValue button1 = new ButtonPushMonitorValue(name: "Button 1 pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 1)
		ButtonPushMonitorValue button2 = new ButtonPushMonitorValue(name: "Button 2 pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 2)
		monitorValueRepository.saveAll([button1, button2])

		MonitorSubscriber button1Subscriber = new MonitorSubscriber(name: "Buttons 1 subscriber", monitor: this.passageButtonPushMonitor,
			handlers: [button1Handler], valueCondition: button1)
		MonitorSubscriber button2Subscriber = new MonitorSubscriber(name: "Buttons 2 subscriber", monitor: this.passageButtonPushMonitor,
				handlers: [button2Handler], valueCondition: button2)

		monitorSubscriberRepository.saveAll([button1Subscriber, button2Subscriber])
	}

	private void motion1()
	{
		ValueReader motionOfPassageMotionDetector = valueReaderRepository.save(
				new ValueReader(name: "Passage Motion", portId: "zwave://${zwaveModem.id}/node/2/ALARM",
						transformer: "if(value.actualType != 0x07) return null; return value.status == 0x08"))
		ValueReader motionOfLandingMotionDetector = valueReaderRepository.save(
				new ValueReader(name: "Landing Motion", portId: "zwave://${zwaveModem.id}/node/7/ALARM",
						transformer: "if(value.actualType != 0x07) return null; return value.status == 0x08"))

		valueReaderRepository.save motionOfPassageMotionDetector
		valueReaderRepository.save motionOfLandingMotionDetector

		Monitor passageOrLandingOccupied = new BooleanMonitor(
				name: "Passage or Landing Occupied", location: house,
				sources: [motionOfPassageMotionDetector, motionOfLandingMotionDetector])

		monitorRepository.save passageOrLandingOccupied

		landingAndPassageOn = actionRepository.save new MacroAction(name: "Turn Landing & Passage Lights On", actions: [setPassageAndLandingToScene])
		landingAndPassageOff = actionRepository.save new MacroAction(name: "Turn Landing & Passage Lights Off", actions: [turnPassageLightOff, turnLandingLightOff])

		MonitorValuePredicate passageLandingLightsAreAuto = new MonitorValuePredicate(name: "Passage/Landing Lights are Auto",
			monitor: passageLandingAutoMonitor, monitorValue: bOn)
		environmentPredicateRepository.save passageLandingLightsAreAuto

		MonitorHandler turnOnPassageAndLandingLights =
				new MonitorHandler(name: "Turn on passage & landing light", action: this.landingAndPassageOn, predicate: passageLandingLightsAreAuto)

		MonitorHandler turnOffPassageAndLandingLights =
				new MonitorHandler(name: "Turn off passage & landing light", action: this.landingAndPassageOff, predicate: passageLandingLightsAreAuto)

		monitorHandlerRepository.save(turnOnPassageAndLandingLights)
		monitorHandlerRepository.save(turnOffPassageAndLandingLights)

//		MonitorValueHandler two = new MonitorValueHandler(handlerConditions: [])

		MonitorSubscriber passageAndLandingOccupied = new MonitorSubscriber(name: "Passage/Landing Occupied", monitor: passageOrLandingOccupied,
				handlers: [turnOnPassageAndLandingLights],
				valueCondition: this.bOn)

		MonitorSubscriber passageAndLandingUnoccpied = new MonitorSubscriber(name: "Passage/Landing Unoccupied", monitor: passageOrLandingOccupied,
				handlers: [turnOffPassageAndLandingLights],
				valueCondition: this.bOff)

		monitorSubscriberRepository.saveAll([passageAndLandingOccupied, passageAndLandingUnoccpied])
	}

	private void eveningButton()
	{
		SetDeviceValueAction lre1NorthCeilingCol = new SetDeviceValueAction(name: "LRE1 North Ceil Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "113,87,61/80", appliance: nceil, portId: "COLOUR")
		SetDeviceValueAction lre1SouthCeilingCol = new SetDeviceValueAction(name: "LRE1 South Ceil Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "193,204,173/80", appliance: sceil, portId: "COLOUR")
		SetDeviceValueAction lre1NorthLampCol = new SetDeviceValueAction(name: "LRE1 North Lamp Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "206,125,53/80", appliance: nlamp, portId: "COLOUR")
		SetDeviceValueAction lre1SouthLampCol = new SetDeviceValueAction(name: "LRE1 South Lamp Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "206,82,44/80", appliance: slamp, portId: "COLOUR")
		SetDeviceValueAction lre1TvLeftCol = new SetDeviceValueAction(name: "LRE1 TV Left Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "198,41,16/80", appliance: tvLeft, portId: "COLOUR")
		SetDeviceValueAction lre1TvRightCol = new SetDeviceValueAction(name: "LRE1 TV Right Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "198,77,39/80", appliance: tvRight, portId: "COLOUR")
		SetDeviceValueAction lre1WindowCol = new SetDeviceValueAction(name: "LRE1 Window Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "198,77,39/80", appliance: window, portId: "COLOUR")

		actionRepository.saveAll([lre1NorthCeilingCol, lre1SouthCeilingCol, lre1NorthLampCol, lre1SouthLampCol,
								  lre1TvLeftCol, lre1TvRightCol, lre1WindowCol])

		SetSceneAction livingRoomSetSceneEvening1 = new SetSceneAction(name: "Living Room Evening 1", deviceValues: [
				lre1NorthCeilingCol, lre1SouthCeilingCol, lre1NorthLampCol, lre1SouthLampCol,
					lre1TvRightCol, lre1TvLeftCol, lre1WindowCol
		])

		SetDeviceValueAction lre2NorthCeilingCol = new SetDeviceValueAction(name: "LRE2 North Ceil Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "113,82,61/44", appliance: nceil, portId: "COLOUR")
		SetDeviceValueAction lre2SouthCeilingCol = new SetDeviceValueAction(name: "LRE2 South Ceil Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "113,82,61/44", appliance: sceil, portId: "COLOUR")
		SetDeviceValueAction lre2NorthLampCol = new SetDeviceValueAction(name: "LRE2 North Lamp Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "84,57,115/45", appliance: nlamp, portId: "COLOUR")
		SetDeviceValueAction lre2SouthLampCol = new SetDeviceValueAction(name: "LRE2 South Lamp Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "115,52,0/45", appliance: slamp, portId: "COLOUR")
		SetDeviceValueAction lre2TvLeftCol = new SetDeviceValueAction(name: "LRE2 TV Left  Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "123,51,0/45", appliance: tvLeft, portId: "COLOUR")
		SetDeviceValueAction lre2TvRightCol = new SetDeviceValueAction(name: "LRE2 TV Right Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "91,63,123/45", appliance: tvRight, portId: "COLOUR")
		SetDeviceValueAction lre2WindowCol = new SetDeviceValueAction(name: "LRE2 Window Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "91,63,123/45", appliance: window, portId: "COLOUR")

		actionRepository.saveAll([lre2NorthCeilingCol, lre2SouthCeilingCol, lre2NorthLampCol, lre2SouthLampCol,
								  lre2TvLeftCol, lre2TvRightCol, lre2WindowCol])

		SetSceneAction livingRoomSetSceneEvening2 = new SetSceneAction(name: "Living Room Evening 2", deviceValues: [
				lre2NorthCeilingCol, lre2SouthCeilingCol, lre2NorthLampCol, lre2SouthLampCol,
					lre2TvLeftCol, lre2TvRightCol, lre2WindowCol
		])

		actionRepository.saveAll([livingRoomSetSceneEvening1, livingRoomSetSceneEvening2])


		SetDeviceValueAction eveningPassageColour = new SetDeviceValueAction(name: "Evening Passage Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "254,222,115/100", appliance: passageLight, portId: "COLOUR")
		SetDeviceValueAction eveningLandingColour = new SetDeviceValueAction(name: "Evening Landing Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "254,222,115/100", appliance: landingLight, portId: "COLOUR")

		actionRepository.saveAll([eveningPassageColour, eveningLandingColour])

		eveningPassageLandingScene = new SetSceneAction(name: "Evening Passage/Landing", deviceValues: [
				eveningPassageColour, eveningLandingColour
		])

		actionRepository.save this.eveningPassageLandingScene


		SetDeviceValueAction dayPassageColour = new SetDeviceValueAction(name: "Day Passage Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100", appliance: passageLight, portId: "COLOUR")
		SetDeviceValueAction dayLandingColour = new SetDeviceValueAction(name: "Day Landing Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100", appliance: landingLight, portId: "COLOUR")

		actionRepository.saveAll([dayPassageColour, dayLandingColour])

		dayPassageLandingScene = new SetSceneAction(name: "Day Passage/Landing", deviceValues: [
				dayPassageColour, dayLandingColour
		])

		actionRepository.save this.dayPassageLandingScene

		dayPassageLandingSceneValue = new SceneMonitorValue(name: "Day scene for Passage/Landing", scene: this.dayPassageLandingScene)
		monitorValueRepository.save this.dayPassageLandingSceneValue

		SceneMonitorValue eveningPassageLandingSceneValue = new SceneMonitorValue(name: "Evening scene for Passage/Landing", scene: this.eveningPassageLandingScene)
		monitorValueRepository.save eveningPassageLandingSceneValue


		passageLandingSceneMonitor = new SceneMonitor(name: "Passage/Landing Scene", defaultValue: this.dayPassageLandingSceneValue)
		monitorRepository.save this.passageLandingSceneMonitor



		SetMonitorValueAction setPassageLandingSceneToEvening =
				new SetMonitorValueAction(name: "Set Passage/Landing Scene to Evening",
						monitor: this.passageLandingSceneMonitor, monitorValue: eveningPassageLandingSceneValue)
		actionRepository.save setPassageLandingSceneToEvening

		SetMonitorValueAction setBedroomSceneToEvening =
				new SetMonitorValueAction(name: "Set Bedroom Scene to Evening",
						monitor: bedroomSceneMonitor, monitorValue: bedroomEveningSceneMonVal)
		actionRepository.save setBedroomSceneToEvening



		Action startEvening1Action = new MacroAction(name: "Activate Evening (1)",
				actions: [setLivingRoomToNoAuto, livingRoomSetSceneEvening1, setPassageLandingToAuto, setPassageLandingSceneToEvening, setBedroomSceneToEvening])
		actionRepository.save(startEvening1Action)

		setPassageAndLandingToScene = new ExecuteMonitorSceneAction(name: "Set Passage/Landing to Scene", monitor: this.passageLandingSceneMonitor)
		actionRepository.save this.setPassageAndLandingToScene

		ButtonPushMonitorValue eveningButtonPushedOnce = new ButtonPushMonitorValue(name: "Evening Button Pushed (1)",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 7)
		monitorValueRepository.saveAll([eveningButtonPushedOnce])

		MonitorHandler eveningButtonHandler = new MonitorHandler(name: "Evening Button Handler", action: startEvening1Action)
		monitorHandlerRepository.saveAll([eveningButtonHandler])


		MonitorSubscriber button1Subscriber = new MonitorSubscriber(name: "Evening Button Subscriber", monitor: this.passageButtonPushMonitor,
				handlers: [eveningButtonHandler], valueCondition: eveningButtonPushedOnce)
		monitorSubscriberRepository.saveAll([button1Subscriber])


		Action startEvening2Action = new MacroAction(name: "Activate Evening (2)",
				actions: [setLivingRoomToNoAuto, livingRoomSetSceneEvening2, setPassageLandingToAuto, setPassageLandingSceneToEvening, setBedroomSceneToEvening])
		actionRepository.save(startEvening2Action)

		ButtonPushMonitorValue eveningButtonPushedTwice = new ButtonPushMonitorValue(name: "Evening Button Pushed (2)",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 2, sceneNumber: 7)
		monitorValueRepository.saveAll([eveningButtonPushedTwice])

		MonitorHandler evening2ButtonHandler = new MonitorHandler(name: "Evening2 Button Handler", action: startEvening2Action)
		monitorHandlerRepository.saveAll([evening2ButtonHandler])


		MonitorSubscriber button2Subscriber = new MonitorSubscriber(name: "Evening2 Button Subscriber", monitor: this.passageButtonPushMonitor,
				handlers: [evening2ButtonHandler], valueCondition: eveningButtonPushedTwice)
		monitorSubscriberRepository.saveAll([button2Subscriber])
	}

	private void doneDownstairs()
	{
		turnOffNorthCeil = new SetDeviceValueAction(name: "Turn off ceiling north", appliance: nceil, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		turnOffSouthCeil = new SetDeviceValueAction(name: "Turn off ceiling south", appliance: sceil, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		turnOffNorthLamp = new SetDeviceValueAction(name: "Turn off lamp north", appliance: nlamp, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		turnOffSouthLamp = new SetDeviceValueAction(name: "Turn off lamp south", appliance: slamp, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		turnOffTvLeft = new SetDeviceValueAction(name: "Turn off TV Left", appliance: tvLeft, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		turnOffTvRight = new SetDeviceValueAction(name: "Turn off TV Right", appliance: tvRight, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		turnOffWindow = new SetDeviceValueAction(name: "Turn off window", appliance: window, portId: "POWER", rawValue: "false", valueType: SetDeviceValueAction.ValueType.BOOL)
		actionRepository.saveAll([this.turnOffNorthCeil, this.turnOffNorthLamp, this.turnOffSouthCeil, this.turnOffSouthLamp,
								  this.turnOffTvLeft, this.turnOffTvRight, this.turnOffWindow])




		Action doneDownstairsAction = new MacroAction(name: "Done downstairs action",
				actions: [setBedroomToNoAuto, this.turnOffNorthCeil, this.turnOffSouthCeil, this.turnOffNorthLamp, this.turnOffSouthLamp, eveningPassageLandingScene,
				          setPassageLandingToNoAuto, bedroomW2B, this.turnOffTvLeft, this.turnOffTvRight, this.turnOffWindow])
		actionRepository.save doneDownstairsAction


		ButtonPushMonitorValue doneDownstairsPushedOnce = new ButtonPushMonitorValue(name: "Done downstairs pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 8)
		monitorValueRepository.saveAll([doneDownstairsPushedOnce])

		MonitorHandler doneDownstairsHandler = new MonitorHandler(name: "Done downstairs handler", action: doneDownstairsAction)
		monitorHandlerRepository.saveAll([doneDownstairsHandler])


		MonitorSubscriber doneDownstairsButtonSubscriber = new MonitorSubscriber(name: "Done Downstairs Button Subscriber", monitor: this.passageButtonPushMonitor,
				handlers: [doneDownstairsHandler], valueCondition: doneDownstairsPushedOnce)

		monitorSubscriberRepository.saveAll([doneDownstairsButtonSubscriber])
	}

	private void autoMonitors()
	{
		passageLandingAutoMonitor = new BooleanMonitor(name: "Passage/Landing Auto", defaultValue: bOn)
		monitorRepository.save passageLandingAutoMonitor

		setPassageLandingToAuto = new SetMonitorValueAction(name: "Set Passage/Landing to Auto",
				monitor: passageLandingAutoMonitor, monitorValue: bOn)
		actionRepository.save this.setPassageLandingToAuto

		setPassageLandingToNoAuto = new SetMonitorValueAction(name: "Set Passage/Landing to No Auto",
				monitor: passageLandingAutoMonitor, monitorValue: bOff)
		actionRepository.save this.setPassageLandingToNoAuto



		bedroomAutoMonitor = new BooleanMonitor(name: "Bedroom Auto", defaultValue: bOn)
		monitorRepository.save bedroomAutoMonitor

		setBedroomToAuto = new SetMonitorValueAction(name: "Set Bedroom to Auto",
				monitor: bedroomAutoMonitor, monitorValue: bOn)
		actionRepository.save this.setBedroomToAuto

		setBedroomToNoAuto = new SetMonitorValueAction(name: "Set Bedroom to No Auto",
				monitor: bedroomAutoMonitor, monitorValue: bOff)
		actionRepository.save this.setBedroomToNoAuto


		livingRoomAutoMonitor = new BooleanMonitor(name: "Living Room Auto", defaultValue: bOn)
		monitorRepository.save livingRoomAutoMonitor

		setLivingRoomToAuto = new SetMonitorValueAction(name: "Set Living Room to Auto",
				monitor: livingRoomAutoMonitor, monitorValue: bOn)
		actionRepository.save setLivingRoomToAuto

		setLivingRoomToNoAuto = new SetMonitorValueAction(name: "Set Living Room to No Auto",
				monitor: livingRoomAutoMonitor, monitorValue: bOff)
		actionRepository.save setLivingRoomToNoAuto
	}

	private void bed1()
	{
		Action bedroomBedCeil = new SetDeviceValueAction(name: "Bedroom evening ceiling", appliance: bedroomCeiling,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "180,82,44/40")
		Action bedroomBedLamp = new SetDeviceValueAction(name: "Bedroom evening lamp", appliance: bedroomThrowLamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "160,82,64/40")
		actionRepository.saveAll([bedroomBedCeil, bedroomBedLamp])

		SetSceneAction bedroomEvening = new SetSceneAction(name: "Bedroom Evening", deviceValues: [bedroomBedCeil, bedroomBedLamp, this.turnOffReadingLight])
		actionRepository.save bedroomEvening

		Action inBed1 = new MacroAction(name: "In bed 1", actions: [setBedroomToNoAuto, setPassageLandingToNoAuto, bedroomEvening, turnLandingLightOff, turnPassageLightOff])
		actionRepository.save inBed1

		ButtonPushMonitorValue inBed1PushedOnce = new ButtonPushMonitorValue(name: "In bed (1) pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 1)
		monitorValueRepository.saveAll([inBed1PushedOnce])

		MonitorHandler inBed1Handler = new MonitorHandler(name: "In bed (1) handler", action: inBed1)
		monitorHandlerRepository.saveAll([inBed1Handler])


		MonitorSubscriber inBed1Subscriber = new MonitorSubscriber(name: "In bed (1) Button Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [inBed1Handler], valueCondition: inBed1PushedOnce)

		monitorSubscriberRepository.saveAll([inBed1Subscriber])

	}

	private void bed2()
	{
		Action inBed2 = new MacroAction(name: "In bed 2", actions: [this.turnOffBedroomCeil, this.turnOffBedroomLamp])
		actionRepository.save inBed2

		ButtonPushMonitorValue inBed2PushedTwice = new ButtonPushMonitorValue(name: "In bed (2) pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 2, sceneNumber: 1)
		monitorValueRepository.saveAll([inBed2PushedTwice])

		MonitorHandler inBed2Handler = new MonitorHandler(name: "In bed (2) handler", action: inBed2)
		monitorHandlerRepository.saveAll([inBed2Handler])


		MonitorSubscriber inBed2Subscriber = new MonitorSubscriber(name: "In bed (2) Button Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [inBed2Handler], valueCondition: inBed2PushedTwice)

		monitorSubscriberRepository.saveAll([inBed2Subscriber])

	}

	private void morning()
	{
		SetDeviceValueAction passageMorning = new SetDeviceValueAction(name: "Passage Morning", appliance: passageLight,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		SetDeviceValueAction landingMorning = new SetDeviceValueAction(name: "Landing Morning", appliance: landingLight,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		actionRepository.saveAll([passageMorning, landingMorning])

		SetSceneAction passageLandingMorningScene = new SetSceneAction(name: "Passage/Landing Morning Scene", deviceValues: [
		        passageMorning, landingMorning
		])
		actionRepository.save passageLandingMorningScene

		SceneMonitorValue passageLandingMorningSceneValue = new SceneMonitorValue(name: "Passage/Landing Morning Scene Value", scene: passageLandingMorningScene)
		monitorValueRepository.save passageLandingMorningSceneValue

		SetMonitorValueAction setPassageLandingSceneToMorning = new SetMonitorValueAction(name: "Set p/l scene to morning",
				monitor: passageLandingSceneMonitor, monitorValue: passageLandingMorningSceneValue)
		actionRepository.save setPassageLandingSceneToMorning



		SetMonitorValueAction setLivingRoomSceneToMorning =
				new SetMonitorValueAction(name: "Set Living Room Scene to Morning",
						monitor: this.livingRoomSceneMonitor, monitorValue: lrMorningSceneVal)
		actionRepository.save  setLivingRoomSceneToMorning


		Action morningAction = new MacroAction(name: "Morning",
				actions: [setLivingRoomToAuto, setBedroomToAuto, setPassageLandingToAuto, setLivingRoomSceneToMorning, lrMorningScene,
				          setPassageLandingSceneToMorning, setBedroomSceneToMorning, bedroomMorningScene])
		actionRepository.save morningAction

		MonitorHandler morningHandler = new MonitorHandler(name: "Morning Handler", action: morningAction)
		monitorHandlerRepository.saveAll([morningHandler])

		ButtonPushMonitorValue bedroomMorningPushed = new ButtonPushMonitorValue(name: "Morning (bedroom) pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 3)
		ButtonPushMonitorValue passageMorningPushed = new ButtonPushMonitorValue(name: "Morning (passage) pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 5)
		monitorValueRepository.saveAll([bedroomMorningPushed, passageMorningPushed])

		MonitorSubscriber bedroomMorningPushedSubscriber = new MonitorSubscriber(name: "Morning (Bedroom) Button Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [morningHandler], valueCondition: bedroomMorningPushed)
		MonitorSubscriber passageMorningPushedSubscriber = new MonitorSubscriber(name: "Morning (Passage) Button Subscriber", monitor: this.passageButtonPushMonitor,
				handlers: [morningHandler], valueCondition: passageMorningPushed)
		monitorSubscriberRepository.saveAll([bedroomMorningPushedSubscriber, passageMorningPushedSubscriber])

	}
	
	private void livingRoomScenes()
	{
		SetDeviceValueAction lrCeilNorthMorning = new SetDeviceValueAction(name: "LR Ceil North Morning", appliance: nceil,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		SetDeviceValueAction lrCeilSouthMorning = new SetDeviceValueAction(name: "LR Ceil South Morning", appliance: sceil,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		SetDeviceValueAction lrLampNorthMorning = new SetDeviceValueAction(name: "LR Lamp North Morning", appliance: nlamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		SetDeviceValueAction lrLampSouthMorning = new SetDeviceValueAction(name: "LR Lamp South Morning", appliance: slamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		actionRepository.saveAll([lrCeilNorthMorning, lrCeilSouthMorning, lrLampNorthMorning, lrLampSouthMorning])

		lrMorningScene = new SetSceneAction(name: "Living Room Morning Scene", deviceValues: [
				lrCeilNorthMorning, lrCeilSouthMorning, lrLampNorthMorning, lrLampSouthMorning
		])
		actionRepository.save(this.lrMorningScene)
		
		lrMorningSceneVal = new SceneMonitorValue(name: "Living Room Morning Scene Val", scene: lrMorningScene)
		monitorValueRepository.save lrMorningSceneVal
		
		SetDeviceValueAction dayLRNorthCeilColour = new SetDeviceValueAction(name: "Day LR Ceil North Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100", appliance: nceil, portId: "COLOUR")
		SetDeviceValueAction dayLRSouthCeilColour = new SetDeviceValueAction(name: "Day LR Ceil South Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100", appliance: sceil, portId: "COLOUR")
		SetDeviceValueAction dayLRNorthLampColour = new SetDeviceValueAction(name: "Day LR Lamp North Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100", appliance: nlamp, portId: "COLOUR")
		SetDeviceValueAction dayLRSouthLampColour = new SetDeviceValueAction(name: "Day LR Lamp South Colour",
				valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100", appliance: slamp, portId: "COLOUR")
		actionRepository.saveAll([dayLRNorthCeilColour, dayLRSouthCeilColour, dayLRNorthLampColour, dayLRSouthLampColour])

		SetSceneAction dayLivingRoomScene = new SetSceneAction(name: "Day Living Room", deviceValues: [
				dayLRNorthCeilColour, dayLRSouthCeilColour, dayLRNorthLampColour, dayLRSouthLampColour
		])
		actionRepository.save dayLivingRoomScene

		MonitorValue livingRoomDaySceneVal = new SceneMonitorValue(name: "Living room Day Scene Monitor Value", scene: dayLivingRoomScene)
		monitorValueRepository.save livingRoomDaySceneVal

		lrDaySceneValue = new SceneMonitorValue(name: "Day scene for Living Room", scene: dayLivingRoomScene)
		monitorValueRepository.save this.lrDaySceneValue

		livingRoomSceneMonitor = new SceneMonitor(name: "Living Room Scene", location: livingRoom, defaultValue: livingRoomDaySceneVal)
		monitorRepository.save this.livingRoomSceneMonitor
	}

	private void day()
	{
		SetMonitorValueAction setPassageLandingSceneToDay =
				new SetMonitorValueAction(name: "Set Passage/Landing Scene to Day",
						monitor: this.passageLandingSceneMonitor, monitorValue: dayPassageLandingSceneValue)
		actionRepository.save  setPassageLandingSceneToDay

		SetMonitorValueAction setLivingRoomSceneToDay =
				new SetMonitorValueAction(name: "Set Living Room Scene to Day",
						monitor: this.livingRoomSceneMonitor, monitorValue: lrDaySceneValue)
		actionRepository.save  setLivingRoomSceneToDay

		Action dayAction = new MacroAction(name: "Day", actions: [
		        setPassageLandingToAuto, setBedroomToAuto, setPassageLandingSceneToDay, setLivingRoomSceneToDay, setBedroomSceneToDay
		])

		actionRepository.save dayAction


		MonitorHandler dayHandler = new MonitorHandler(name: "Day Handler", action: dayAction)
		monitorHandlerRepository.saveAll([dayHandler])

		Action turnOffLivingRoomLights = new MacroAction(name: "Turn off living room lights", actions: [
		        turnOffSouthCeil, turnOffNorthCeil, turnOffNorthLamp, turnOffSouthLamp
		])
		actionRepository.save turnOffLivingRoomLights

		Action turnOnLivingRoomLights = new ExecuteMonitorSceneAction(name: "xmsa for turn on lr lights", monitor: this.livingRoomSceneMonitor)
		actionRepository.save(turnOnLivingRoomLights)

		MonitorValuePredicate isDaylightPred = new MonitorValuePredicate(monitor: daylightMonitor, monitorValue: bOn)
		environmentPredicateRepository.save(isDaylightPred)
		MonitorValuePredicate isNotDaylightPred = new MonitorValuePredicate(monitor: daylightMonitor, monitorValue: bOff)
		environmentPredicateRepository.save(isNotDaylightPred)

		MonitorHandler lrLightsForDayHandlerOn = new MonitorHandler(name: "Living Room Lights on for day handler",
				action: turnOnLivingRoomLights, predicate: isNotDaylightPred)
		MonitorHandler lrLightsForDayHandlerOff = new MonitorHandler(name: "Living Room Lights off for day handler",
				action: turnOffLivingRoomLights, predicate: isDaylightPred)
		MonitorHandler bedroomLightsForDayHandlerOn = new MonitorHandler(name: "Bedroom Lights on for day handler",
				action: bedroomLightsOn, predicate: isNotDaylightPred)
		MonitorHandler bedroomLightsForDayHandlerOff = new MonitorHandler(name: "Bedroom Lights off for day handler",
				action: bedroomLightsOff, predicate: isDaylightPred)
		MonitorHandler passageLandingLightsForDayHandlerOn = new MonitorHandler(name: "Passage/Landing Lights on for day handler",
				action: setPassageAndLandingToScene)
		monitorHandlerRepository.saveAll([lrLightsForDayHandlerOn, lrLightsForDayHandlerOff, bedroomLightsForDayHandlerOn,
		                                  bedroomLightsForDayHandlerOff, passageLandingLightsForDayHandlerOn])

		ButtonPushMonitorValue bedroomDayPushed = new ButtonPushMonitorValue(name: "Day (bedroom) pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 4)
		ButtonPushMonitorValue passageDayPushed = new ButtonPushMonitorValue(name: "Day (passage) pushed",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 6)
		monitorValueRepository.saveAll([bedroomDayPushed, passageDayPushed])

		MonitorSubscriber bedroomDayPushedSubscriber = new MonitorSubscriber(name: "Day (Bedroom) Button Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [dayHandler, lrLightsForDayHandlerOn, lrLightsForDayHandlerOff, bedroomLightsForDayHandlerOn,
				           bedroomLightsForDayHandlerOff, passageLandingLightsForDayHandlerOn,],
				valueCondition: bedroomDayPushed)
		MonitorSubscriber passageDayPushedSubscriber = new MonitorSubscriber(name: "Day (Passage) Button Subscriber", monitor: this.passageButtonPushMonitor,
				handlers: [dayHandler, lrLightsForDayHandlerOn, lrLightsForDayHandlerOff, bedroomLightsForDayHandlerOn,
				           bedroomLightsForDayHandlerOff, passageLandingLightsForDayHandlerOn,], valueCondition: passageDayPushed)
		monitorSubscriberRepository.saveAll([bedroomDayPushedSubscriber, passageDayPushedSubscriber])


		MonitorValuePredicate isLivingRoomAuto = new MonitorValuePredicate(monitor: livingRoomAutoMonitor, monitorValue: bOn)
		environmentPredicateRepository.save(isLivingRoomAuto)

		MonitorHandler lightsOnAtDuskHandler = new MonitorHandler(name: "Lights on for dusk", action: turnOnLivingRoomLights, predicate: isLivingRoomAuto)
		monitorHandlerRepository.save(lightsOnAtDuskHandler)

		MonitorSubscriber daylightSubscriber = new MonitorSubscriber(name: "Daylight Subscriber", monitor: daylightMonitor,
			valueCondition: bOff, handlers: [lightsOnAtDuskHandler])
		monitorSubscriberRepository.save(daylightSubscriber)


		MonitorHandler lightsOffAtDawnHandler = new MonitorHandler(name: "Lights off for dawn", action: turnOffLivingRoomLights, predicate: isLivingRoomAuto)
		monitorHandlerRepository.save(lightsOffAtDawnHandler)

		MonitorSubscriber daylightOnSubscriber = new MonitorSubscriber(name: "Daylight Subscriber", monitor: daylightMonitor,
				valueCondition: bOn, handlers: [lightsOffAtDawnHandler])
		monitorSubscriberRepository.save(daylightOnSubscriber)


//		Thread.start {
//			Thread.sleep(5_000)
//			log.error("NO DAYLIGHT NOW!")
//			actionContext.setMonitorValue(daylightMonitor, bOff)
//		}
	}

	private void nightlights()
	{
		Action setLandingToNightlight = new SetDeviceValueAction(name: "Landing Nightlight On", appliance: landingLight,
				valueType: SetDeviceValueAction.ValueType.COLOUR, portId: "COLOUR", rawValue: "255,0,0/20")
		Action setPassageToNightlight = new SetDeviceValueAction(name: "Passage Nightlight On", appliance: passageLight,
				valueType: SetDeviceValueAction.ValueType.COLOUR, portId: "COLOUR", rawValue: "255,0,0/20")

		Action setLRCeilNorthToNightlight = new SetDeviceValueAction(name: "LR Ceiling North Nightlight On", appliance: nceil,
				valueType: SetDeviceValueAction.ValueType.COLOUR, portId: "COLOUR", rawValue: "255,0,0/20")
		Action setLRCeilSouthToNightlight = new SetDeviceValueAction(name: "LR Ceiling South Nightlight On", appliance: sceil,
				valueType: SetDeviceValueAction.ValueType.COLOUR, portId: "COLOUR", rawValue: "255,0,0/20")
		Action setLRLampNorthToNightlight = new SetDeviceValueAction(name: "LR Lamp North Nightlight On", appliance: nlamp,
				valueType: SetDeviceValueAction.ValueType.COLOUR, portId: "COLOUR", rawValue: "255,0,0/20")
		Action setLRLampSouthToNightlight = new SetDeviceValueAction(name: "LR Lamp South Nightlight On", appliance: slamp,
				valueType: SetDeviceValueAction.ValueType.COLOUR, portId: "COLOUR", rawValue: "255,0,0/20")
		actionRepository.saveAll([setLandingToNightlight, setPassageToNightlight, setLRCeilNorthToNightlight, setLRCeilSouthToNightlight, setLRLampNorthToNightlight, setLRLampSouthToNightlight])

		Action setToiletNightlightScene = new SetSceneAction(name: "nl1?", deviceValues: [setLandingToNightlight])
		Action setDownstairsNightlightScene = new SetSceneAction(name: "nl2?", deviceValues: [setLandingToNightlight, setPassageToNightlight, setLRCeilNorthToNightlight, setLRCeilSouthToNightlight, setLRLampNorthToNightlight, setLRLampSouthToNightlight])
		actionRepository.saveAll([setToiletNightlightScene, setDownstairsNightlightScene])

		Action turnToiletNightlightOff = new MacroAction(name: "Turn toilet nightlight off", actions: [
		        turnLandingLightOff
		])
		Action turnDownstairsNightlightOff = new MacroAction(name: "Turn downstairs nightlight off", actions: [
				turnLandingLightOff, turnPassageLightOff, turnOffSouthLamp, turnOffNorthLamp, turnOffNorthCeil, turnOffSouthCeil
		])
		actionRepository.saveAll([turnToiletNightlightOff, turnDownstairsNightlightOff])

		MonitorHandler toiletNightlightOnHandler = new MonitorHandler(name: "Toilet Nightlight On Handler", action: setToiletNightlightScene)
		MonitorHandler toiletNightlightOffHandler = new MonitorHandler(name: "Toilet Nightlight Off Handler", action: turnToiletNightlightOff)
		MonitorHandler downstairsNightlightOnHandler = new MonitorHandler(name: "Downstairs Nightlight On Handler", action: setDownstairsNightlightScene)
		MonitorHandler downstairsNightlightOffHandler = new MonitorHandler(name: "Downstairs Nightlight Off Handler", action: turnDownstairsNightlightOff)
		monitorHandlerRepository.saveAll([toiletNightlightOnHandler, toiletNightlightOffHandler, downstairsNightlightOnHandler, downstairsNightlightOffHandler])

		ButtonPushMonitorValue toiletNightlightOn = new ButtonPushMonitorValue(name: "Toilet nightlight on",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 5)
		ButtonPushMonitorValue toiletNightlightOff = new ButtonPushMonitorValue(name: "Toilet nightlight off",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 2, sceneNumber: 5)
		ButtonPushMonitorValue downstairsNightlightOn = new ButtonPushMonitorValue(name: "Downstairs nightlight on",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 6)
		ButtonPushMonitorValue downstairsNightlightOff = new ButtonPushMonitorValue(name: "Downstairs nightlight off",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 2, sceneNumber: 6)
		monitorValueRepository.saveAll([toiletNightlightOn, toiletNightlightOff, downstairsNightlightOn, downstairsNightlightOff])

		MonitorSubscriber toiletNightlightOnSubscriber = new MonitorSubscriber(name: "Toilet Nightlight On Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [toiletNightlightOnHandler], valueCondition: toiletNightlightOn)
		MonitorSubscriber toiletNightlightOffSubscriber = new MonitorSubscriber(name: "Toilet Nightlight Off Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [toiletNightlightOffHandler], valueCondition: toiletNightlightOff)
		MonitorSubscriber downstairsNightlightOnSubscriber = new MonitorSubscriber(name: "Downstairs Nightlight On Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [downstairsNightlightOnHandler], valueCondition: downstairsNightlightOn)
		MonitorSubscriber downstairsNightlightOffSubscriber = new MonitorSubscriber(name: "Downstairs Nightlight Off Subscriber", monitor: this.bedroomButtonPushMonitor,
				handlers: [downstairsNightlightOffHandler], valueCondition: downstairsNightlightOff)
		monitorSubscriberRepository.saveAll([toiletNightlightOnSubscriber, toiletNightlightOffSubscriber, downstairsNightlightOnSubscriber, downstairsNightlightOffSubscriber])

	}

	void readingLight()
	{

		SetDeviceValueAction readingCoolBrightValue = new SetDeviceValueAction(name: "Reading Cool Bright Value", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "153/100")
		SetDeviceValueAction readingCoolDimValue = new SetDeviceValueAction(name: "Reading Cool Dim Value", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "153/50")
		SetDeviceValueAction readingWarmBrightValue = new SetDeviceValueAction(name: "Reading Warm Bright Value", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "500/100")
		SetDeviceValueAction readingWarmDimValue = new SetDeviceValueAction(name: "Reading Warm Dim Value", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "500/50")
		actionRepository.saveAll([readingCoolBrightValue, readingCoolDimValue, readingWarmBrightValue, readingWarmDimValue])

		SetSceneAction readingCoolBright = new SetSceneAction(name: "Reading Cool Bright", deviceValues: [readingCoolBrightValue])
		SetSceneAction readingCoolDim = new SetSceneAction(name: "Reading Cool Dim", deviceValues: [readingCoolDimValue])
		SetSceneAction readingWarmBright = new SetSceneAction(name: "Reading Warm Bright", deviceValues: [readingWarmBrightValue])
		SetSceneAction readingWarmDim = new SetSceneAction(name: "Reading Warm Dim", deviceValues: [readingWarmDimValue])
		actionRepository.saveAll([readingCoolBright, readingCoolDim, readingWarmBright, readingWarmDim])

		MonitorValue readingCoolBrightMonVal = new SceneMonitorValue(name: "Reading Cool Bright", scene: readingCoolBright)
		MonitorValue readingCoolDimMonVal = new SceneMonitorValue(name: "Reading Cool Dim", scene: readingCoolDim)
		MonitorValue readingWarmBrightMonVal = new SceneMonitorValue(name: "Reading Warm Bright", scene: readingWarmBright)
		MonitorValue readingWarmDimMonVal = new SceneMonitorValue(name: "Reading Warm Dim", scene: readingWarmDim)
		monitorValueRepository.saveAll([readingCoolBrightMonVal, readingCoolDimMonVal, readingWarmBrightMonVal, readingWarmDimMonVal])

		Monitor readingLightSceneMonitor = new SceneMonitor(name: "Reading Light Scene", location: bedroom, defaultValue: readingWarmBrightMonVal)
		monitorRepository.save(readingLightSceneMonitor)

		ButtonPushMonitorValue readingLightOnButtonValue = new ButtonPushMonitorValue(name: "Reading Light On",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 1, sceneNumber: 2)
		ButtonPushMonitorValue readingLightOffButtonValue = new ButtonPushMonitorValue(name: "Reading Light Off",
				type: ButtonPushMonitorValue.Type.PUSHED, pushCount: 2, sceneNumber: 2)
		ButtonPushMonitorValue readingLightSceneButtonValue = new ButtonPushMonitorValue(name: "Reading Light Scene Change",
				type: ButtonPushMonitorValue.Type.RELEASED_AFTER_HOLD, sceneNumber: 2)
		monitorValueRepository.saveAll([readingLightOnButtonValue, readingLightOffButtonValue, readingLightSceneButtonValue])

		Monitor readingLightOnButtonMonitor = new ButtonPushMonitor(name: "Reading Light On Button", sources: [bedroomButtonsReader], location: bedroom)
		Monitor readingLightOffButtonMonitor = new ButtonPushMonitor(name: "Reading Light Off Button", sources: [bedroomButtonsReader], location: bedroom)
		Monitor readingLightSceneButtonMonitor = new ButtonPushMonitor(name: "Reading Light Scene Button", sources: [bedroomButtonsReader], location: bedroom)
		monitorRepository.saveAll([readingLightOnButtonMonitor, readingLightOffButtonMonitor, readingLightSceneButtonMonitor])



		Action turnOnReadingLight = new ExecuteMonitorSceneAction(name: "Turn on reading light", monitor: readingLightSceneMonitor)
		Action turnOffReadingLight = new SetDeviceValueAction(name: "Turn off reading light", appliance: readingLight,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")


		Action setReadingLightSceneToCoolBright = new SetMonitorValueAction(name: "Set Reading light scene to cool bright",
				monitor: readingLightSceneMonitor, monitorValue: readingCoolBrightMonVal)
		Action setReadingLightSceneToCoolDim = new SetMonitorValueAction(name: "Set Reading light scene to cool dim",
				monitor: readingLightSceneMonitor, monitorValue: readingCoolDimMonVal)
		Action setReadingLightSceneToWarmBright = new SetMonitorValueAction(name: "Set Reading light scene to warm bright",
				monitor: readingLightSceneMonitor, monitorValue: readingWarmBrightMonVal)
		Action setReadingLightSceneToWarmDim = new SetMonitorValueAction(name: "Set Reading light scene to warm dim",
				monitor: readingLightSceneMonitor, monitorValue: readingWarmDimMonVal)

		actionRepository.saveAll([turnOnReadingLight, turnOffReadingLight, setReadingLightSceneToCoolBright,
		                          setReadingLightSceneToCoolDim, setReadingLightSceneToWarmBright,
		                          setReadingLightSceneToWarmDim])

		EnvironmentPredicate readingLightIsCoolBright = new MonitorValuePredicate(name: "Reading light is cool bright?",
				monitor: readingLightSceneMonitor, monitorValue: readingCoolBrightMonVal)
		EnvironmentPredicate readingLightIsCoolDim = new MonitorValuePredicate(name: "Reading light is cool dim?",
				monitor: readingLightSceneMonitor, monitorValue: readingCoolDimMonVal)
		EnvironmentPredicate readingLightIsWarmBright = new MonitorValuePredicate(name: "Reading light is warm bright?",
				monitor: readingLightSceneMonitor, monitorValue: readingWarmBrightMonVal)
		EnvironmentPredicate readingLightIsWarmDim = new MonitorValuePredicate(name: "Reading light is warm dim?",
				monitor: readingLightSceneMonitor, monitorValue: readingWarmDimMonVal)
		environmentPredicateRepository.saveAll([readingLightIsCoolBright, readingLightIsCoolDim, readingLightIsWarmBright, readingLightIsWarmDim])


		MonitorHandler readingLightOnHandler = new MonitorHandler(name: "Reading Light On Handler", action: turnOnReadingLight)
		MonitorHandler readingLightOffHandler = new MonitorHandler(name: "Reading Light Off Handler", action: turnOffReadingLight)
		MonitorHandler readingLightSceneChange1 = new MonitorHandler(name: "Reading Light Scene to Cool Bright",
				action: setReadingLightSceneToCoolBright, predicate: readingLightIsWarmDim)
		MonitorHandler readingLightSceneChange2 = new MonitorHandler(name: "Reading Light Scene to Cool Dim",
				action: setReadingLightSceneToCoolDim, predicate: readingLightIsCoolBright)
		MonitorHandler readingLightSceneChange3 = new MonitorHandler(name: "Reading Light Scene to Warm Bright",
				action: setReadingLightSceneToWarmBright, predicate: readingLightIsCoolDim)
		MonitorHandler readingLightSceneChange4 = new MonitorHandler(name: "Reading Light Scene to Warm Dim",
				action: setReadingLightSceneToWarmDim, predicate: readingLightIsWarmBright)
		monitorHandlerRepository.saveAll([readingLightOnHandler, readingLightOffHandler, readingLightSceneChange1,
		                                  readingLightSceneChange2, readingLightSceneChange3, readingLightSceneChange4])


		MonitorSubscriber readlingLightOnSubscriber = new MonitorSubscriber(name: "Reading Light On Subscriber",
				monitor: readingLightOnButtonMonitor, handlers: [readingLightOnHandler], valueCondition: readingLightOnButtonValue)
		MonitorSubscriber readlingLightOffSubscriber = new MonitorSubscriber(name: "Reading Light Off Subscriber",
				monitor: readingLightOnButtonMonitor, handlers: [readingLightOffHandler], valueCondition: readingLightOffButtonValue)
		MonitorSubscriber readlingLightSceneSubscriber = new MonitorSubscriber(name: "Reading Light Scene Subscriber",
				monitor: readingLightOnButtonMonitor,
				handlers: [readingLightSceneChange1, readingLightSceneChange2, readingLightSceneChange3, readingLightSceneChange4, readingLightOnHandler],
				valueCondition: readingLightSceneButtonValue)
		monitorSubscriberRepository.saveAll([readlingLightOnSubscriber, readlingLightOffSubscriber, readlingLightSceneSubscriber])
	}

	void bedroomMotion()
	{
		ValueReader motionOfBedroomMotionDetector = valueReaderRepository.save(
				new ValueReader(name: "Passage Motion", portId: "zwave://${zwaveModem.id}/node/9/ALARM",
						transformer: "if(value.actualType != 0x07) return null; return value.status == 0x08"))

		valueReaderRepository.save motionOfBedroomMotionDetector

		Monitor bedroomOcccupied = new BooleanMonitor(
				name: "Bedroom Occupied", location: bedroom,
				sources: [motionOfBedroomMotionDetector])
		monitorRepository.save bedroomOcccupied


		bedroomLightsOn = actionRepository.save new ExecuteMonitorSceneAction(name: "Turn Bedroom Lights On", monitor: this.bedroomSceneMonitor)
		bedroomLightsOff = actionRepository.save new MacroAction(name: "Turn Bedroom Lights Off", actions: [turnOffBedroomLamp, turnOffBedroomCeil, turnOffReadingLight])
		actionRepository.saveAll([this.bedroomLightsOn, this.bedroomLightsOff])

		MonitorValuePredicate bedroomLightsAreAuto = new MonitorValuePredicate(name: "Bedroom Lights are Auto",
				monitor: bedroomAutoMonitor, monitorValue: bOn)
		environmentPredicateRepository.save bedroomLightsAreAuto

		AndPredicate autoAndNotDaylight = new AndPredicate(name: "Bedroom Auto and Not Daylight", predicates: [bedroomLightsAreAuto]) // TODO
		environmentPredicateRepository.save autoAndNotDaylight

		MonitorHandler turnOnBedroomLightsHandler =
				new MonitorHandler(name: "Turn on bedroom light", action: this.bedroomLightsOn, predicate: bedroomLightsAreAuto)

		MonitorHandler turnOffBedroomLightsHandler =
				new MonitorHandler(name: "Turn off bedroom light", action: this.bedroomLightsOff, predicate: bedroomLightsAreAuto)

		monitorHandlerRepository.save(turnOnBedroomLightsHandler)
		monitorHandlerRepository.save(turnOffBedroomLightsHandler)

		MonitorSubscriber bedroomOccSubscriber = new MonitorSubscriber(name: "Bedroom Occupied", monitor: bedroomOcccupied,
				handlers: [turnOnBedroomLightsHandler],
				valueCondition: this.bOn)

		MonitorSubscriber bedroomUnoccSubscriber = new MonitorSubscriber(name: "Bedroom Unoccupied", monitor: bedroomOcccupied,
				handlers: [turnOffBedroomLightsHandler],
				valueCondition: this.bOff)

		monitorSubscriberRepository.saveAll([bedroomOccSubscriber, bedroomUnoccSubscriber])
	}

	void bedroomScenes()
	{
		turnOffBedroomCeil = new SetDeviceValueAction(name: "Bedroom Ceiling Off", appliance: bedroomCeiling,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")
		turnOffBedroomLamp = new SetDeviceValueAction(name: "Bedroom Throw Lamp Off", appliance: bedroomThrowLamp,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")
		actionRepository.saveAll([this.turnOffBedroomCeil, this.turnOffBedroomLamp])

		Action bedroomMorningCeil = new SetDeviceValueAction(name: "Bedroom morning ceiling", appliance: this.bedroomCeiling,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		Action bedroomMorningLamp = new SetDeviceValueAction(name: "Bedroom morning lamp", appliance: this.bedroomThrowLamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,0/100")
		Action bedroomMorningReadingLamp =  new SetDeviceValueAction(name: "Bedroom morning reading lamp", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "500/70")
		actionRepository.saveAll([bedroomMorningCeil, bedroomMorningLamp, bedroomMorningReadingLamp])


		bedroomMorningScene = new SetSceneAction(name: "Bedroom Morning Scene", deviceValues: [bedroomMorningCeil, bedroomMorningLamp, bedroomMorningReadingLamp])
		actionRepository.save this.bedroomMorningScene

		SceneMonitorValue bedroomMorningSceneVal = new SceneMonitorValue(name: "Bedroom morning scene val", scene: this.bedroomMorningScene)
		monitorValueRepository.save bedroomMorningSceneVal




		Action bedroomDayCeil = new SetDeviceValueAction(name: "Bedroom evening ceiling", appliance: this.bedroomCeiling,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100")
		Action bedroomDayLamp = new SetDeviceValueAction(name: "Bedroom evening lamp", appliance: this.bedroomThrowLamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "255,255,255/100")
		Action bedroomDayReadingLamp =  new SetDeviceValueAction(name: "Bedroom evening reading lamp", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "153/100")

		Action bedroomEveningCeil = new SetDeviceValueAction(name: "Bedroom evening ceiling", appliance: this.bedroomCeiling,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "254,222,115/90")
		Action bedroomEveningLamp = new SetDeviceValueAction(name: "Bedroom evening lamp", appliance: this.bedroomThrowLamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "254,222,115/90")
		Action bedroomEveningReadingLamp =  new SetDeviceValueAction(name: "Bedroom evening reading lamp", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "500/100")
		actionRepository.saveAll([bedroomDayCeil, bedroomDayLamp, bedroomDayReadingLamp, bedroomEveningCeil, bedroomEveningLamp, bedroomEveningReadingLamp])


		SetSceneAction bedroomDayScene = new SetSceneAction(name: "Bedroom Day Scene", deviceValues: [bedroomDayCeil, bedroomDayLamp, bedroomDayReadingLamp])
		SetSceneAction bedroomEveningScene = new SetSceneAction(name: "Bedroom Evening Scene", deviceValues: [bedroomEveningCeil, bedroomEveningLamp, bedroomEveningReadingLamp])
		actionRepository.saveAll([bedroomDayScene, bedroomEveningScene])

		SceneMonitorValue bedroomDaySceneMonVal = new SceneMonitorValue(name: "Bedroom Day Scene Val", scene: bedroomDayScene)
		bedroomEveningSceneMonVal = new SceneMonitorValue(name: "Bedroom Evening Scene Val", scene: bedroomEveningScene)
		monitorValueRepository.saveAll([bedroomDaySceneMonVal, this.bedroomEveningSceneMonVal])

		bedroomSceneMonitor = new SceneMonitor(name: "Bedroom Scene", location: bedroom, defaultValue: bedroomDaySceneMonVal)
		monitorRepository.save this.bedroomSceneMonitor


		setBedroomSceneToMorning = new SetMonitorValueAction(name: "Set bedroom scene to morning",
				monitor: bedroomSceneMonitor, monitorValue: bedroomMorningSceneVal)
		actionRepository.save this.setBedroomSceneToMorning

		setBedroomSceneToDay = new SetMonitorValueAction(name: "Set bedroom scene to day",
				monitor: bedroomSceneMonitor, monitorValue: bedroomDaySceneMonVal)
		actionRepository.save setBedroomSceneToDay


		Action bedroomW2BCeil = new SetDeviceValueAction(name: "Bedroom W2B ceiling", appliance: this.bedroomCeiling,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "180,82,44/70")
		Action bedroomW2BEveningLamp = new SetDeviceValueAction(name: "Bedroom W2B lamp", appliance: this.bedroomThrowLamp,
				portId: "COLOUR", valueType: SetDeviceValueAction.ValueType.COLOUR, rawValue: "160,82,64/70")
		Action bedroomW2BReadingLamp =  new SetDeviceValueAction(name: "Bedroom W2B reading lamp", appliance: readingLight,
				portId: "CT", valueType: SetDeviceValueAction.ValueType.CT, rawValue: "500/40")
		actionRepository.saveAll([bedroomW2BCeil, bedroomW2BEveningLamp, bedroomW2BReadingLamp])

		bedroomW2B = new SetSceneAction(name: "Bedroom W2B", deviceValues: [bedroomW2BCeil, bedroomW2BEveningLamp, bedroomW2BReadingLamp])
		actionRepository.save this.bedroomW2B


		turnOffReadingLight = new SetDeviceValueAction(name: "reading light off DUPLICATE DELETE ME", appliance: readingLight,
				portId: "POWER", valueType: SetDeviceValueAction.ValueType.BOOL, rawValue: "false")
		actionRepository.save turnOffReadingLight
	}

}
