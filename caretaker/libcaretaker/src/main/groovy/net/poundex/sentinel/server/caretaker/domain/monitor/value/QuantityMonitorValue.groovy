package net.poundex.sentinel.server.caretaker.domain.monitor.value

import groovy.transform.ToString
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.Entity
import java.util.function.BiFunction

@Entity
@ToString(includeSuperProperties = true, includeNames = true, includePackage = false)
class QuantityMonitorValue extends MonitorValue
{
	double quantityValue
	CompareType compareType = CompareType.EQUALS

	static QuantityMonitorValue of(String name, double value) {
		return new QuantityMonitorValue().tap {
			it.name = name
			it.quantityValue = value
		}
	}

	@Override
	boolean test(MonitorValue monitorValue)
	{
		if(monitorValue instanceof QuantityMonitorValue)
			return compareType.apply(this.quantityValue, monitorValue.quantityValue)

		return false
	}

	enum CompareType {
		EQUALS({ v1, v2 -> v1 == v2 }),
		GT({ v1, v2 -> v2 > v1 }),
		LT(null),
		GTE(null),
		LTE(null)

		private final BiFunction<Double, Double, Boolean> fn

		CompareType(BiFunction<Double, Double, Boolean> fn)
		{
			this.fn = fn
		}

		boolean apply(Double monitorValue1, Double monitorValue2) {
			return fn.apply(monitorValue1, monitorValue2)
		}
	}
}
