package net.poundex.sentinel.server.caretaker.enviornment

import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

interface Environment
{
	MonitorValue getMonitorValue(Monitor monitor)
}