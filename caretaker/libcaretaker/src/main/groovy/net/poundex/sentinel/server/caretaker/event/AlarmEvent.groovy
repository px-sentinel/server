package net.poundex.sentinel.server.caretaker.event

import groovy.transform.Canonical

@Canonical
class AlarmEvent
{
	byte type
	byte level
	byte actualType
	byte status
}
