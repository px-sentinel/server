package net.poundex.sentinel.server.caretaker.domain.monitor

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.value.ButtonPushMonitorValue
import net.poundex.sentinel.server.caretaker.event.ButtonPushEvent

import javax.persistence.Entity

@Entity
@Slf4j
class ButtonPushMonitor extends Monitor
{
	@Override
	MonitorValue getValueForInputs(Collection<ReadDeviceValue> readDeviceValues)
	{
		log.info("ButtonPushMonitor: getValueForInputs({})", readDeviceValues)
		ButtonPushEvent value = readDeviceValues.first().value
		return new ButtonPushMonitorValue(pushCount: value.pushCount, sceneNumber: value.sceneNumber, type: value.type)
	}
}
