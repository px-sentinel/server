package net.poundex.sentinel.server.caretaker.device.value

interface ReadWriteValuePort extends ReadableValuePort, WritableValuePort
{
}
