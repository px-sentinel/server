package net.poundex.sentinel.server.caretaker.domain.action

import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import net.poundex.sentinel.server.caretaker.domain.monitor.SceneMonitor
import net.poundex.sentinel.server.caretaker.domain.monitor.value.SceneMonitorValue

import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
class ExecuteMonitorSceneAction extends Action
{
	@OneToOne
	SceneMonitor monitor

	@Override
	void run(ActionContext actionContext)
	{
		SceneMonitorValue smv = actionContext.environment.getMonitorValue(monitor) as SceneMonitorValue
		smv.scene.run(actionContext)
	}
}
