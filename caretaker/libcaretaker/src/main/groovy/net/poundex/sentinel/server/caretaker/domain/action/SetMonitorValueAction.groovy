package net.poundex.sentinel.server.caretaker.domain.action

import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
class SetMonitorValueAction extends Action
{
	@OneToOne
	Monitor monitor

	@OneToOne
	MonitorValue monitorValue

	@Override
	void run(ActionContext actionContext)
	{
		actionContext.setMonitorValue(monitor, monitorValue)
	}
}
