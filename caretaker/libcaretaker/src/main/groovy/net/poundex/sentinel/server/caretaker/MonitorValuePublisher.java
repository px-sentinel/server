package net.poundex.sentinel.server.caretaker;

import net.poundex.sentinel.server.caretaker.domain.Monitor;
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue;

public interface MonitorValuePublisher
{
	void publish(Monitor monitor, MonitorValue monitorValue);
}
