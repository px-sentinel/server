package net.poundex.sentinel.server.caretaker.domain

import groovy.transform.EqualsAndHashCode
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@EqualsAndHashCode(includes = "id")
class ValueReader
{
	@Id @GeneratedValue
	Long id
	String name
	String transformer
	String portId

	ReadDeviceValue read(DeviceValue deviceValue) {
		return new ReadDeviceValue(deviceValue, this, readValue(deviceValue.value))
	}

	Object readValue(Object rawValue) {
		if( ! transformer)
			return rawValue
		return Eval.me("value", rawValue, transformer)
	}

	@Override
	String toString() {
		return "Reads ${portId}"
	}
}
