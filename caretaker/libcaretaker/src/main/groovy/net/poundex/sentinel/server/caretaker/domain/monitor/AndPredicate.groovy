package net.poundex.sentinel.server.caretaker.domain.monitor

import net.poundex.sentinel.server.caretaker.enviornment.Environment

import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.OneToMany

@Entity
class AndPredicate extends EnvironmentPredicate
{
	@OneToMany(fetch = FetchType.EAGER)
	Set<EnvironmentPredicate> predicates = new HashSet<>()

	static AndPredicate of(MonitorValuePredicate... predicates)
	{
		return new AndPredicate().tap {
			it.predicates.addAll(predicates)
		}
	}

	@Override
	boolean test(Environment environment)
	{
		return predicates.every { it.test(environment) }
	}
}
