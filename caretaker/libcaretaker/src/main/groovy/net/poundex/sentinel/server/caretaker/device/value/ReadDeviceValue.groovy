package net.poundex.sentinel.server.caretaker.device.value

import groovy.transform.ToString
import net.poundex.sentinel.server.caretaker.domain.ValueReader

@ToString(includePackage = false)
class ReadDeviceValue
{
	final DeviceValue raw
	final ValueReader valueReader
	final Object value

	ReadDeviceValue(DeviceValue raw, ValueReader valueReader, Object value)
	{
		this.raw = raw
		this.valueReader = valueReader
		this.value = value
	}
}
