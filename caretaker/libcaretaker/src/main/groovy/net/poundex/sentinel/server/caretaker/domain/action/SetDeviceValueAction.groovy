package net.poundex.sentinel.server.caretaker.domain.action

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.domain.appliance.Appliance

import javax.persistence.Entity
import javax.persistence.OneToOne
import java.util.function.Function

@Entity
@Slf4j
class SetDeviceValueAction extends Action
{
	@OneToOne
	Appliance appliance

	// TODO this is port name, not port id
	String portId
	ValueType valueType
	String rawValue

	@Override
	void run(ActionContext actionContext)
	{
		actionContext.setDeviceValue(appliance, portId, valueType.getValue(rawValue))
	}

	enum ValueType {
		BOOL({ v -> Boolean.valueOf(v) }),
		COLOUR({ v -> v }),
		CT({v -> v})

		private final Function<String, Object> fn

		ValueType(Function<String, Object> fn)
		{
			this.fn = fn
		}

		Object getValue(String raw)
		{
			return fn.apply(raw)
		}
	}
}
