package net.poundex.sentinel.server.caretaker.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
class Zone
{
	@Id @GeneratedValue
	Long id

	String name
	@OneToOne
	Zone parent
}
