package net.poundex.sentinel.server.caretaker.domain

import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.BooleanMonitorValue

import javax.persistence.Entity
import java.util.function.Function

@Entity
class BooleanMonitor extends Monitor
{
	Aggregator aggregator = Aggregator.ANY

	@Override
	BooleanMonitorValue getValueForInputs(Collection<ReadDeviceValue> readDeviceValues)
	{
		return new BooleanMonitorValue(boolValue: aggregator.aggregate(readDeviceValues))
	}

	enum Aggregator {
		NEWEST_VALUE({ Collection<ReadDeviceValue> vals -> return vals.max { it.raw.time }}),
		ANY({ Collection<ReadDeviceValue> vals -> return vals.any { it.value } }),
		EVERY({ Collection<ReadDeviceValue> vals -> return vals.every { it.value } })

		private final Function<Collection<ReadDeviceValue>, Boolean> aggregateFn

		Aggregator(Function<Collection<ReadDeviceValue>, Boolean> aggregateFn)
		{
			this.aggregateFn = aggregateFn
		}

		Boolean aggregate(Collection<ReadDeviceValue> readDeviceValues)
		{
			return aggregateFn.apply(readDeviceValues)
		}
	}
}
