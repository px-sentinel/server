package net.poundex.sentinel.server.caretaker.domain.monitor

import net.poundex.sentinel.server.caretaker.domain.Monitor

import javax.persistence.*

@Entity
class MonitorSubscriber 
{
	@Id @GeneratedValue
	Long id

	String name
	
	@OneToOne
	Monitor monitor
	
	@ManyToMany(fetch = FetchType.EAGER)
	@OrderColumn
	List<MonitorHandler> handlers = []
	
	@OneToOne
	MonitorValue valueCondition

	boolean isInterestedIn(MonitorValue monitorValue) {
		return ! valueCondition || valueCondition.test(monitorValue)
	}
}
