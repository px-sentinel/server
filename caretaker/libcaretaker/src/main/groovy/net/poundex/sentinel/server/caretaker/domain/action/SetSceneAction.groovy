package net.poundex.sentinel.server.caretaker.domain.action

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.ActionContext

import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.ManyToMany
import javax.persistence.OrderColumn

@Entity
@Slf4j
class SetSceneAction extends Action
{
	@ManyToMany(fetch = FetchType.EAGER)
	@OrderColumn
	List<SetDeviceValueAction> deviceValues = []

	@Override
	void run(ActionContext actionContext)
	{
		deviceValues*.run(actionContext)
	}

}
