package net.poundex.sentinel.server.caretaker.domain

import groovy.transform.EqualsAndHashCode
import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.*

@Entity
@EqualsAndHashCode(includes = "id")
abstract class Monitor
{
	@Id @GeneratedValue
	Long id

	String name

	@ManyToMany
	Set<ValueReader> sources = new HashSet<>()

	@OneToOne
	Zone location

	@OneToOne
	MonitorValue defaultValue

	@Override
	String toString() {
		return name
	}

	abstract MonitorValue getValueForInputs(Collection<ReadDeviceValue> readDeviceValues)
}
