package net.poundex.sentinel.server.caretaker.domain.monitor

import net.poundex.sentinel.server.caretaker.enviornment.Environment

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
abstract class EnvironmentPredicate
{
	@Id @GeneratedValue
	Long id
	
	String name

	abstract boolean test(Environment environment)
}
