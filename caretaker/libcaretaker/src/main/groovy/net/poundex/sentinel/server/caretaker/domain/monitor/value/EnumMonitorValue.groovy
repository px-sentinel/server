package net.poundex.sentinel.server.caretaker.domain.monitor.value

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.Entity

@Entity
@EqualsAndHashCode(includes = ["valueSet", "enumValue"])
@ToString(includeSuperProperties = true, includeNames = true, includePackage = false)
class EnumMonitorValue extends MonitorValue
{
	String valueSet
	String enumValue

	static EnumMonitorValue of(String name, String valueSet, String value) {
		return new EnumMonitorValue().tap {
			it.name = name
			it.valueSet = valueSet
			it.enumValue = value
		}
	}

	@Override
	boolean test(MonitorValue monitorValue)
	{
		return this == monitorValue
	}
}
