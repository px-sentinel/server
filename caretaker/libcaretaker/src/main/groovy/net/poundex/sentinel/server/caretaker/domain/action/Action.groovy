package net.poundex.sentinel.server.caretaker.domain.action

import groovy.transform.EqualsAndHashCode
import net.poundex.sentinel.server.caretaker.ActionContext

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@EqualsAndHashCode(includes = "id")
abstract class Action
{
	@Id @GeneratedValue
	Long id

	String name

	abstract void run(ActionContext actionContext)
}
