package net.poundex.sentinel.server.caretaker

import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.appliance.Appliance
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import net.poundex.sentinel.server.caretaker.enviornment.Environment

interface ActionContext
{
	void setDeviceValue(Appliance appliance, String portId, Object value)
	void setMonitorValue(Monitor monitor, MonitorValue monitorValue)
	Environment getEnvironment()
}
