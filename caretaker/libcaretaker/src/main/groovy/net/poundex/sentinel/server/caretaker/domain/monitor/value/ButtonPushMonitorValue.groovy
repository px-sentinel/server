package net.poundex.sentinel.server.caretaker.domain.monitor.value

import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.Entity

@Entity
class ButtonPushMonitorValue extends MonitorValue
{
	Integer sceneNumber
	Type type
	Integer pushCount

	@Override
	boolean test(MonitorValue monitorValue)
	{
		if(monitorValue instanceof ButtonPushMonitorValue)
			return monitorValue.sceneNumber == sceneNumber &&
					monitorValue.type == type &&
					monitorValue.pushCount == pushCount
		return false
	}

	static enum Type
	{
		PUSHED, BEING_HELD_DOWN, RELEASED_AFTER_HOLD
	}

	@Override
	String toString() {
		return "Button #${sceneNumber} ${type} Count:${pushCount}"
	}
}
