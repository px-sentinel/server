package net.poundex.sentinel.server.caretaker.domain.action

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.ActionContext

import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.ManyToMany

@Entity
@Slf4j
class MacroAction extends Action
{
	@ManyToMany(fetch = FetchType.EAGER)
	Set<Action> actions = new HashSet<>()

	@Override
	void run(ActionContext actionContext)
	{
		actions*.run(actionContext)
	}
}
