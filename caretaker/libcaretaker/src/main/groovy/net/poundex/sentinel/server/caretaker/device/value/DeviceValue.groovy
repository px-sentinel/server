package net.poundex.sentinel.server.caretaker.device.value

import groovy.transform.Canonical
import groovy.transform.CompileStatic
import groovy.transform.ToString

import java.time.Instant

@Canonical
@ToString(includePackage = false)
@CompileStatic
class DeviceValue
{
	String deviceId
	String portName
	Object value
	Instant time
	Map<String, ?> extraOpts = [:]

	String getPortId()
	{
		return deviceId.toURI().resolve(portName).toString()
	}
}
