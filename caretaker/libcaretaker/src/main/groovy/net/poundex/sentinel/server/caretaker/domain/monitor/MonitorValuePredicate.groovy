package net.poundex.sentinel.server.caretaker.domain.monitor

import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.enviornment.Environment

import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
class MonitorValuePredicate extends EnvironmentPredicate
{
	@OneToOne
	Monitor monitor
	
	@OneToOne
	MonitorValue monitorValue

	@Override
	boolean test(Environment environment)
	{
		return environment.getMonitorValue(monitor).test(monitorValue)
	}
}
