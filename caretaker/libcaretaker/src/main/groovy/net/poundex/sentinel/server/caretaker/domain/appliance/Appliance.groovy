package net.poundex.sentinel.server.caretaker.domain.appliance

import net.poundex.sentinel.server.caretaker.domain.Zone

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Inheritance
import javax.persistence.InheritanceType
import javax.persistence.OneToOne

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
class Appliance
{
	@Id @GeneratedValue
	Long id

	String name
	String deviceId
	@OneToOne
	Zone location
}
