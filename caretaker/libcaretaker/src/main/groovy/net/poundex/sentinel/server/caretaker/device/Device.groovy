package net.poundex.sentinel.server.caretaker.device

import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort

interface Device
{
	String getDeviceId()
	Hardware getHardware()
	String getName()
	Set<ValuePort> getValuePorts()
	void setValue(DeviceValue deviceValue)
}
