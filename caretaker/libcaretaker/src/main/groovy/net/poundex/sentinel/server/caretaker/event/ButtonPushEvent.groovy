package net.poundex.sentinel.server.caretaker.event

import groovy.transform.Canonical
import groovy.transform.ToString
import net.poundex.sentinel.server.caretaker.domain.monitor.value.ButtonPushMonitorValue

@Canonical
@ToString(includePackage = false)
class ButtonPushEvent
{
	ButtonPushMonitorValue.Type type
	Integer pushCount
	Integer sceneNumber
}
