package net.poundex.sentinel.server.caretaker.domain

import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.QuantityMonitorValue

import javax.persistence.Entity
import java.util.function.Function

@Entity
class QuantityMonitor extends Monitor
{
	Aggregator aggregator = Aggregator.AVERAGE

	@Override
	QuantityMonitorValue getValueForInputs(Collection<ReadDeviceValue> readDeviceValues)
	{
		return QuantityMonitorValue.of("", /* TODO */ aggregator.aggregate(readDeviceValues) as double)
	}

	enum Aggregator {
		NEWEST_VALUE({ Collection<ReadDeviceValue> vals -> return vals.max { it.raw.time }}),
		AVERAGE({ Collection<ReadDeviceValue> vals -> return vals.sum { it.value } / vals.size() })

		private final Function<Collection<ReadDeviceValue>, ?> aggregateFn

		Aggregator(Function<Collection<ReadDeviceValue>, ?> aggregateFn)
		{
			this.aggregateFn = aggregateFn
		}

		Object aggregate(Collection<ReadDeviceValue> readDeviceValues)
		{
			return aggregateFn.apply(readDeviceValues)
		}
	}
}
