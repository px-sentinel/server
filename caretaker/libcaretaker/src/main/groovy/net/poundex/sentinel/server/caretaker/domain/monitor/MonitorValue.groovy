package net.poundex.sentinel.server.caretaker.domain.monitor


import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class MonitorValue
{
	@Id @GeneratedValue
	Long id
	
	String name

	abstract boolean test(MonitorValue monitorValue)
}
