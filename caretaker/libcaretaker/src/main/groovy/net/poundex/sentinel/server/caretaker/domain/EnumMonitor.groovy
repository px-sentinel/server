package net.poundex.sentinel.server.caretaker.domain

import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.EnumMonitorValue

import javax.persistence.Entity
import java.util.function.Function

@Entity
class EnumMonitor extends Monitor
{
	Class<? extends Enum> enumClass

	@Override
	EnumMonitorValue getValueForInputs(Collection<ReadDeviceValue> readDeviceValues)
	{
		return null 
	}
}
