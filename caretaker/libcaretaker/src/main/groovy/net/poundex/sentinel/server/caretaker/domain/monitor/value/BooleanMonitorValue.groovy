package net.poundex.sentinel.server.caretaker.domain.monitor.value

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.Entity

@Entity
@EqualsAndHashCode(includes = "boolValue")
@ToString(includeSuperProperties = true, includeNames = true, includePackage = false)
class BooleanMonitorValue extends MonitorValue
{
	boolean boolValue

	static BooleanMonitorValue of(String name, boolean value) {
		return new BooleanMonitorValue().tap {
			it.name = name
			it.boolValue = value
		}
	}

	@Override
	boolean test(MonitorValue monitorValue)
	{
		if(monitorValue instanceof BooleanMonitorValue)
			return monitorValue.boolValue == this.boolValue
		return false
	}

	@Override
	String toString() {
		return boolValue ? "On" : "Off"
	}
}
