package net.poundex.sentinel.server.caretaker.controller

import org.springframework.data.repository.CrudRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

abstract class ResourceController<T>
{
	protected final Class<T> resourceClass
	protected final CrudRepository<T, Long> repository

	ResourceController( Class<T> resourceClass, CrudRepository<T, Long> repository)
	{
		this.resourceClass = resourceClass
		this.repository = repository
	}

	@GetMapping
	ResponseEntity index() {
		return ResponseEntity.ok(repository.findAll())
	}

	@PostMapping
	ResponseEntity save(@RequestBody Map<String, ?> payload) {
		T obj = resourceClass.newInstance()
		bind(obj, payload)
		return ResponseEntity.ok(repository.save(obj))
	}

	@PutMapping("/{id}")
	ResponseEntity update(@PathVariable Long id, @RequestBody Map<String, ?> payload) {
		T obj = repository.findById(id).orElseThrow({ new RuntimeException() })
		bind(obj, payload)
		return ResponseEntity.ok(repository.save(obj))
	}

	@DeleteMapping("/{id}")
	ResponseEntity delete(@PathVariable Long id) {
		T obj = repository.findById(id).orElseThrow({ new RuntimeException() })
		repository.delete(obj)
		return ResponseEntity.noContent().build()
	}

	protected abstract void bind(T obj, Map<String, ?> payload)
}
