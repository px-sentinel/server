package net.poundex.sentinel.server.caretaker.enviornment

enum Daypart {
	MORNING, DAY, EVENING, NIGHT
}