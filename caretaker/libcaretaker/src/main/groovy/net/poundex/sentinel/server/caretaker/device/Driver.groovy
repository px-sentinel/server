package net.poundex.sentinel.server.caretaker.device

import net.poundex.sentinel.server.caretaker.DeviceManager

interface Driver
{
	void start(DeviceManager deviceManager)
}
