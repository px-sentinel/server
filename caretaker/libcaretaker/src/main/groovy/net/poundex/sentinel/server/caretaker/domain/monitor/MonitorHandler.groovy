package net.poundex.sentinel.server.caretaker.domain.monitor

import net.poundex.sentinel.server.caretaker.domain.action.Action

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
class MonitorHandler 
{
	@Id @GeneratedValue
	Long id

	String name
	
	@OneToOne
	EnvironmentPredicate predicate

	@OneToOne
	Action action
}
