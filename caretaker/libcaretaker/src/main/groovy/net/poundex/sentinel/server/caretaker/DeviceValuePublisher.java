package net.poundex.sentinel.server.caretaker;

import net.poundex.sentinel.server.caretaker.device.value.DeviceValue;

public interface DeviceValuePublisher
{
	void publish(DeviceValue deviceValue);
}
