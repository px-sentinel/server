package net.poundex.sentinel.server.caretaker.domain.monitor.value


import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

@Singleton
class NullMonitorValue extends MonitorValue
{
	@Override
	boolean test(MonitorValue monitorValue)
	{
		return monitorValue instanceof NullMonitorValue
	}

	@Override
	String toString() {
		return "null"
	}
}
