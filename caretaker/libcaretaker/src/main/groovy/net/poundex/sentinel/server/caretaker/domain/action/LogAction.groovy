package net.poundex.sentinel.server.caretaker.domain.action

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.ActionContext

import javax.persistence.Entity

@Entity
@Slf4j
class LogAction extends Action
{
	@Override
	void run(ActionContext actionContext)
	{
		log.warn("Running Action {}", name)
	}
}
