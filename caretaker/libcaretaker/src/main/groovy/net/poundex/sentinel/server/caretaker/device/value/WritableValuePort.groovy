package net.poundex.sentinel.server.caretaker.device.value

interface WritableValuePort extends ValuePort
{
	void setValue(DeviceValue value)
}
