package net.poundex.sentinel.server.caretaker.domain.monitor.value

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import net.poundex.sentinel.server.caretaker.domain.action.SetSceneAction
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue

import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
@EqualsAndHashCode(includes = "scene")
@ToString(includeSuperProperties = true, includeNames = true, includePackage = false)
class SceneMonitorValue extends MonitorValue
{
	@OneToOne
	SetSceneAction scene

	static SceneMonitorValue of(String name, SetSceneAction value) {
		return new SceneMonitorValue().tap {
			it.name = name
			it.scene = value
		}
	}

	@Override
	boolean test(MonitorValue monitorValue)
	{
		return monitorValue == this
	}

	@Override
	String toString() {
		return "Scene: ${scene.name}"
	}
}
