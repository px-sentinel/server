package net.poundex.sentinel.server.caretaker

import net.poundex.sentinel.server.caretaker.device.Device
import org.springframework.http.ResponseEntity

interface DeviceManager
{

	void reset()

	Set<Device> getAllDevices()

	void setDeviceValue(URI deviceId, Object value)

	void register(Device device)
}
