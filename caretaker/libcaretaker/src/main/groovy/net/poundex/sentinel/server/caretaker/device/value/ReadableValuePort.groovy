package net.poundex.sentinel.server.caretaker.device.value

interface ReadableValuePort extends ValuePort
{
	DeviceValue getValue()
}
