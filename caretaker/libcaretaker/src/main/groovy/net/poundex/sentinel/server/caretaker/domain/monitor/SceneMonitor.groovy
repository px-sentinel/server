package net.poundex.sentinel.server.caretaker.domain.monitor

import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.value.SceneMonitorValue

import javax.persistence.Entity

@Entity
class SceneMonitor extends Monitor
{
	@Override
	SceneMonitorValue getValueForInputs(Collection<ReadDeviceValue> readDeviceValues)
	{
		null
	}
}
