package net.poundex.sentinel.server.caretaker.device.value

interface ValuePort
{
	String getId()
	String getType()
}
