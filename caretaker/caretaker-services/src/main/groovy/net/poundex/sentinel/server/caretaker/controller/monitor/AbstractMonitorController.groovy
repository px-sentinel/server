package net.poundex.sentinel.server.caretaker.controller.monitor

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.ZoneRepository
import net.poundex.sentinel.server.caretaker.domain.Monitor
import org.springframework.data.repository.CrudRepository

abstract class AbstractMonitorController<T extends Monitor> extends ResourceController<T>
{
	private final ZoneRepository zoneRepository

	AbstractMonitorController(Class<T> monitorClass, CrudRepository<T, Long> repository, ZoneRepository zoneRepository)
	{
		super(monitorClass, repository)
		this.zoneRepository = zoneRepository
	}

	abstract void bindInternal(T obj, Map<String, ?> payload)
	@Override
	protected void bind(T obj, Map<String, ?> payload)
	{
		obj.name = payload.name
		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
		bindInternal(obj, payload)
	}
}
