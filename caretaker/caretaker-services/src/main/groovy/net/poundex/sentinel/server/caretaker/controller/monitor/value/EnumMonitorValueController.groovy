package net.poundex.sentinel.server.caretaker.controller.monitor.value

import net.poundex.sentinel.server.caretaker.controller.monitor.value.AbstractMonitorValueController
import net.poundex.sentinel.server.caretaker.dao.EnumMonitorValueRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.value.EnumMonitorValue
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitorValue/enum")
class EnumMonitorValueController extends AbstractMonitorValueController<EnumMonitorValue>
{
	EnumMonitorValueController(EnumMonitorValueRepository repository)
	{
		super(EnumMonitorValue, repository)
	}

	@Override
	void bindInternal(EnumMonitorValue obj, Map<String, ?> payload)
	{
	}
}
