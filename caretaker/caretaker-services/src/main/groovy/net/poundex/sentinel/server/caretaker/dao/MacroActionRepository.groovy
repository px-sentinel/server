package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.action.MacroAction
import org.springframework.data.repository.PagingAndSortingRepository

interface MacroActionRepository extends PagingAndSortingRepository<MacroAction, Long> { }
