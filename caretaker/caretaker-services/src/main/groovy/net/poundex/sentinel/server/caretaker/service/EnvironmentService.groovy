package net.poundex.sentinel.server.caretaker.service

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import net.poundex.sentinel.server.caretaker.domain.monitor.value.NullMonitorValue
import net.poundex.sentinel.server.caretaker.enviornment.Environment
import org.springframework.messaging.Message
import org.springframework.stereotype.Service

import java.util.concurrent.ConcurrentHashMap

@Service
@Slf4j
@CompileStatic
class EnvironmentService implements Environment
{
	private final Map<Monitor, MonitorValue> monitorValues = new ConcurrentHashMap<>()
	
	void receiveMonitorValue(Message<MonitorValue> message)
	{
		Monitor m = message.headers.get("Monitor", Monitor)
		Map tmp = monitorValues + [(m): monitorValues[m] ?: NullMonitorValue.instance]
		String newVals = tmp.collect { k, v ->
			" - " + (k == m ? "[*] ${k} = ${v} <== ${message.payload}" : "${k} = ${v}")
		}.join("\n")
		log.warn("Monitor values updated: \n{}", newVals)
		monitorValues[m] = message.payload
	}

	@Override
	MonitorValue getMonitorValue(Monitor monitor) 
	{
		if(monitorValues[monitor] == null && monitor.defaultValue != null)
			monitorValues[monitor] = monitor.defaultValue

		return monitorValues[monitor]
	}
}
