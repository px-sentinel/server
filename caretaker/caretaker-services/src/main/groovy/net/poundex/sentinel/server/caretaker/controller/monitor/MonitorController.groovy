package net.poundex.sentinel.server.caretaker.controller.monitor

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.MonitorRepository
import net.poundex.sentinel.server.caretaker.domain.Monitor
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitor")
class MonitorController extends ResourceController<Monitor>
{

	MonitorController(MonitorRepository monitorRepository)
	{
		super(Monitor, monitorRepository)
	}

	@Override
	ResponseEntity save(@RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	ResponseEntity update(@PathVariable Long id, @RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	protected void bind(Monitor obj, Map<String, ?> payload)
	{
		throw new UnsupportedOperationException()
	}
}
