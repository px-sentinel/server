package net.poundex.sentinel.server.caretaker.controller.monitor.value

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.MonitorValueRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitorValue")
class MonitorValueController extends ResourceController<MonitorValue>
{

	MonitorValueController(MonitorValueRepository repository)
	{
		super(MonitorValue, repository)
	}

	@Override
	ResponseEntity save(@RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	ResponseEntity update(@PathVariable Long id, @RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	protected void bind(MonitorValue obj, Map<String, ?> payload)
	{
		throw new UnsupportedOperationException()
	}
}
