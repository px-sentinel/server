package net.poundex.sentinel.server.caretaker.controller.monitor.envpred

import net.poundex.sentinel.server.caretaker.dao.AndEnvironmentPredicateRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.AndPredicate
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/environmentPredicate/and")
class AndEnvironmentPredicateController extends AbstractEnvironmentPredicateController<AndPredicate>
{
	AndEnvironmentPredicateController(AndEnvironmentPredicateRepository repository)
	{
		super(AndPredicate, repository)
	}

	@Override
	void bindInternal(AndPredicate obj, Map<String, ?> payload)
	{
	}
}
