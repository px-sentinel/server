package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.monitor.value.BooleanMonitorValue
import org.springframework.data.repository.PagingAndSortingRepository

interface BooleanMonitorValueRepository extends PagingAndSortingRepository<BooleanMonitorValue, Long> { }
