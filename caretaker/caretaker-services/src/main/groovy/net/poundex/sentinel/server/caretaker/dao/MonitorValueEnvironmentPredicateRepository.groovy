package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValuePredicate
import org.springframework.data.repository.PagingAndSortingRepository

interface MonitorValueEnvironmentPredicateRepository extends PagingAndSortingRepository<MonitorValuePredicate, Long>
{

}
