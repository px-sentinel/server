package net.poundex.sentinel.server.caretaker.controller.action


import net.poundex.sentinel.server.caretaker.dao.LogActionRepository
import net.poundex.sentinel.server.caretaker.domain.action.LogAction
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/action/log")
class LogActionController extends AbstractActionController<LogAction>
{
	LogActionController(LogActionRepository logActionRepository)
	{
		super(LogAction, logActionRepository)
	}

	@Override
	void bindInternal(LogAction obj, Map<String, ?> payload)
	{
//		obj.aggregator = BooleanMonitor.Aggregator.valueOf(payload.aggregator)
	}
}
