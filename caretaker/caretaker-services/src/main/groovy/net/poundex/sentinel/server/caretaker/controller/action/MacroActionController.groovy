package net.poundex.sentinel.server.caretaker.controller.action


import net.poundex.sentinel.server.caretaker.dao.MacroActionRepository
import net.poundex.sentinel.server.caretaker.domain.action.MacroAction
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/action/macro")
class MacroActionController extends AbstractActionController<MacroAction>
{
	MacroActionController(MacroActionRepository repository)
	{
		super(MacroAction, repository)
	}

	@Override
	void bindInternal(MacroAction obj, Map<String, ?> payload)
	{
//		obj.aggregator = BooleanMonitor.Aggregator.valueOf(payload.aggregator)
	}
}
