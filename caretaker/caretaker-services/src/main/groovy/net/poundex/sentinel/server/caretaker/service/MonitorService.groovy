package net.poundex.sentinel.server.caretaker.service

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.MonitorValuePublisher
import net.poundex.sentinel.server.caretaker.dao.MonitorRepository
import net.poundex.sentinel.server.caretaker.dao.MonitorSubscriberRepository
import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.ValueReader
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import net.poundex.sentinel.server.caretaker.enviornment.Environment
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Service

import java.util.concurrent.ConcurrentHashMap

@Service
@Slf4j
//@CompileStatic
class MonitorService
{
	private final MonitorRepository monitorRepository
	private final MonitorSubscriberRepository monitorSubscriberRepository
	private final MessageChannel monitorValueChannel
	private final Environment environment
	private final ActionContext actionContext
	private final MonitorValuePublisher monitorValuePublisher

	private final Map<Monitor, Map<ValueReader, ReadDeviceValue>> monitorInputValues =
			new ConcurrentHashMap<>()
	private final Map<Monitor, MonitorValue> monitorValues = new ConcurrentHashMap<>()

	MonitorService(MonitorRepository monitorRepository, MonitorSubscriberRepository monitorSubscriberRepository, MessageChannel monitorValueChannel, Environment environment, ActionContext actionContext, MonitorValuePublisher monitorValuePublisher)
	{
		this.monitorRepository = monitorRepository
		this.monitorSubscriberRepository = monitorSubscriberRepository
		this.monitorValueChannel = monitorValueChannel
		this.environment = environment
		this.actionContext = actionContext
		this.monitorValuePublisher = monitorValuePublisher
	}

	void handleReadValue(ReadDeviceValue readDeviceValue)
	{
		monitorRepository.findAllBySources(readDeviceValue.valueReader).each { monitor ->
			if( ! monitorInputValues[monitor])
				monitorInputValues[monitor] = new ConcurrentHashMap<ValueReader, ReadDeviceValue>()

			monitorInputValues[monitor][readDeviceValue.valueReader] = readDeviceValue
			monitorValues[monitor] = monitor.getValueForInputs(monitorInputValues[monitor].values())
			// TODO - Dirty checking, not publish for all???
			monitorValuePublisher.publish(monitor, monitorValues[monitor])
		}
	}
	
	void handleMonitorValue(Message<MonitorValue> monitorValueMessage)
	{
		monitorSubscriberRepository.findAllByMonitor(monitorValueMessage.headers.get("Monitor") as Monitor).findAll { subscriber ->
			subscriber.isInterestedIn(monitorValueMessage.payload)
		}.each { subscriber ->
			subscriber.handlers.findAll { handler ->
				! handler.predicate || handler.predicate.test(environment)
			}.each { handler ->
				log.info("Running action ${handler.action.name} for ${handler.name}")
				handler.action.run(actionContext)
			}
		}
	}

}
