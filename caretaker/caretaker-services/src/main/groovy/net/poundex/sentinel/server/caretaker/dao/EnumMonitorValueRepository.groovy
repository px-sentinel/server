package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.monitor.value.EnumMonitorValue
import org.springframework.data.repository.PagingAndSortingRepository

interface EnumMonitorValueRepository extends PagingAndSortingRepository<EnumMonitorValue, Long> { }
