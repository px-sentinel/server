package net.poundex.sentinel.server.caretaker.service

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.DeviceValuePublisher
import net.poundex.sentinel.server.caretaker.dao.ValueReaderRepository
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ReadDeviceValue
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.GenericMessage
import org.springframework.stereotype.Service

@Service
@Slf4j
@CompileStatic
class ValueService implements DeviceValuePublisher
{
	private final ValueReaderRepository valueReaderRepository
	private final MessageChannel readValueChannel
	private final MessageChannel deviceValueChannel

	ValueService(ValueReaderRepository valueReaderRepository, MessageChannel readValueChannel, MessageChannel deviceValueChannel)
	{
		this.valueReaderRepository = valueReaderRepository
		this.readValueChannel = readValueChannel
		this.deviceValueChannel = deviceValueChannel
	}

	@ServiceActivator
	void handleIncomingValue(DeviceValue deviceValue) {
		valueReaderRepository.findAllByPortId(deviceValue.portId).each { reader ->
			ReadDeviceValue rdv = reader.read(deviceValue)
			if( ! rdv || rdv.value == null) return
			readValueChannel.send(new GenericMessage<ReadDeviceValue>(rdv))
		}
	}

	@Override
	void publish(DeviceValue deviceValue)
	{
		log.info("Publishing device value: {}", deviceValue)
		deviceValueChannel.send(new GenericMessage<DeviceValue>(deviceValue))
	}
}
