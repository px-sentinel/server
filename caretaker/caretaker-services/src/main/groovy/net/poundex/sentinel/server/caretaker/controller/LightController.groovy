package net.poundex.sentinel.server.caretaker.controller

import net.poundex.sentinel.server.caretaker.dao.LightRepository
import net.poundex.sentinel.server.caretaker.dao.ZoneRepository
import net.poundex.sentinel.server.caretaker.domain.appliance.Light
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/light")
class LightController extends ResourceController<Light>
{
	private final ZoneRepository zoneRepository

	LightController(LightRepository lightRepository, ZoneRepository zoneRepository)
	{
		super(Light, lightRepository)
		this.zoneRepository = zoneRepository
	}

	@Override
	protected void bind(Light obj, Map<String, ?> payload) {
		obj.name = payload.name
		obj.deviceId = payload.deviceId
		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
	}
}
