package net.poundex.sentinel.server.caretaker.controller.monitor

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.ValueReaderRepository
import net.poundex.sentinel.server.caretaker.domain.ValueReader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/valueReader")
class ValueReaderController extends ResourceController<ValueReader>
{
	ValueReaderController(ValueReaderRepository valueReaderRepository)
	{
		super(ValueReader, valueReaderRepository)
	}

	@Override
	protected void bind(ValueReader obj, Map<String, ?> payload) {
		obj.name = payload.name
		obj.transformer = payload.transformer
		obj.portId = payload.valueSource

//		obj.deviceId = payload.deviceId
//		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
	}
}
