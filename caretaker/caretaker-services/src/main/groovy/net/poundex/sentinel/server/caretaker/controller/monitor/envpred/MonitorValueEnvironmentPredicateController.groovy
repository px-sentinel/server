package net.poundex.sentinel.server.caretaker.controller.monitor.envpred


import net.poundex.sentinel.server.caretaker.dao.MonitorValueEnvironmentPredicateRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValuePredicate
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/environmentPredicate/monitorValue")
class MonitorValueEnvironmentPredicateController extends AbstractEnvironmentPredicateController<MonitorValuePredicate>
{
	MonitorValueEnvironmentPredicateController(MonitorValueEnvironmentPredicateRepository repository)
	{
		super(MonitorValuePredicate, repository)
	}

	@Override
	void bindInternal(MonitorValuePredicate obj, Map<String, ?> payload)
	{
	}
}
