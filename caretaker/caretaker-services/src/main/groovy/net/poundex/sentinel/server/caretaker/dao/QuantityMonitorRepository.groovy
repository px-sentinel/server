package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.QuantityMonitor
import org.springframework.data.repository.PagingAndSortingRepository

interface QuantityMonitorRepository extends PagingAndSortingRepository<QuantityMonitor, Long> { }
