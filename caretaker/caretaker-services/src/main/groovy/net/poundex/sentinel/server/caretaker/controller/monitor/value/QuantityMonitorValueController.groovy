package net.poundex.sentinel.server.caretaker.controller.monitor.value

import net.poundex.sentinel.server.caretaker.controller.monitor.value.AbstractMonitorValueController
import net.poundex.sentinel.server.caretaker.dao.QuantityMonitorValueRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.value.QuantityMonitorValue
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitorValue/quantity")
class QuantityMonitorValueController extends AbstractMonitorValueController<QuantityMonitorValue>
{
	QuantityMonitorValueController(QuantityMonitorValueRepository repository)
	{
		super(QuantityMonitorValue, repository)
	}

	@Override
	void bindInternal(QuantityMonitorValue obj, Map<String, ?> payload)
	{
	}
}
