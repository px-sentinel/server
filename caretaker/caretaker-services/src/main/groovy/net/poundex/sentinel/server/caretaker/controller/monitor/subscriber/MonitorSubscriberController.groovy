package net.poundex.sentinel.server.caretaker.controller.monitor.subscriber

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.MonitorSubscriberRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorSubscriber
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitorSubscriber")
class MonitorSubscriberController extends ResourceController<MonitorSubscriber>
{
	MonitorSubscriberController(MonitorSubscriberRepository repository)
	{
		super(MonitorSubscriber, repository)
	}

	@Override
	protected void bind(MonitorSubscriber obj, Map<String, ?> payload) {
//		obj.name = payload.name
//		obj.deviceId = payload.deviceId
//		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
	}
}
