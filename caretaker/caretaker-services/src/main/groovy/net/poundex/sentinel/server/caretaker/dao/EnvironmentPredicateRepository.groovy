package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.monitor.EnvironmentPredicate
import org.springframework.data.repository.PagingAndSortingRepository

interface EnvironmentPredicateRepository extends PagingAndSortingRepository<EnvironmentPredicate, Long>
{

}
