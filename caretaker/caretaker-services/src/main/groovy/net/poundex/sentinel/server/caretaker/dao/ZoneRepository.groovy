package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.Zone
import org.springframework.data.repository.PagingAndSortingRepository

//@RepositoryRestResource
interface ZoneRepository extends PagingAndSortingRepository<Zone, Long>
{
}
