package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.ValueReader
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorSubscriber
import org.springframework.data.repository.PagingAndSortingRepository

interface MonitorSubscriberRepository extends PagingAndSortingRepository<MonitorSubscriber, Long>
{
	List<MonitorSubscriber> findAllByMonitor(Monitor monitor)
}
