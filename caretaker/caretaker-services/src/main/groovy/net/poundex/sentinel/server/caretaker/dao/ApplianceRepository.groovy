package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.appliance.Appliance
import org.springframework.data.repository.PagingAndSortingRepository

interface ApplianceRepository extends PagingAndSortingRepository<Appliance, Long>
{
}
