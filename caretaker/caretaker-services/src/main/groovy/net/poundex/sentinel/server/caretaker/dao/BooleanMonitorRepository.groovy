package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.BooleanMonitor
import org.springframework.data.repository.PagingAndSortingRepository

interface BooleanMonitorRepository extends PagingAndSortingRepository<BooleanMonitor, Long> { }
