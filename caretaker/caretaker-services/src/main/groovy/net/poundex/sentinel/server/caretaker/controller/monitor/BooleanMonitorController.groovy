package net.poundex.sentinel.server.caretaker.controller.monitor

import net.poundex.sentinel.server.caretaker.controller.monitor.AbstractMonitorController
import net.poundex.sentinel.server.caretaker.dao.BooleanMonitorRepository
import net.poundex.sentinel.server.caretaker.dao.ZoneRepository
import net.poundex.sentinel.server.caretaker.domain.BooleanMonitor
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitor/boolean")
class BooleanMonitorController extends AbstractMonitorController<BooleanMonitor>
{
	BooleanMonitorController(BooleanMonitorRepository monitorRepository, ZoneRepository zoneRepository)
	{
		super(BooleanMonitor, monitorRepository, zoneRepository)
	}

	@Override
	void bindInternal(BooleanMonitor obj, Map<String, ?> payload)
	{
		obj.aggregator = BooleanMonitor.Aggregator.valueOf(payload.aggregator)
	}
}
