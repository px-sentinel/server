package net.poundex.sentinel.server.caretaker.controller.action

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.domain.action.Action
import org.springframework.data.repository.CrudRepository

abstract class AbstractActionController<T extends Action> extends ResourceController<T>
{
	AbstractActionController(Class<T> monitorClass, CrudRepository<T, Long> repository)
	{
		super(monitorClass, repository)
	}

	abstract void bindInternal(T obj, Map<String, ?> payload)
	@Override
	protected void bind(T obj, Map<String, ?> payload)
	{
		obj.name = payload.name
		bindInternal(obj, payload)
	}
}
