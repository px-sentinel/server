package net.poundex.sentinel.server.caretaker.controller.action

import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.ActionRepository
import net.poundex.sentinel.server.caretaker.domain.action.Action
import net.poundex.sentinel.server.caretaker.enviornment.Environment
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/action")
class ActionController extends ResourceController<Action>
{
	private final Environment environment
	private final ActionContext actionContext

	ActionController(ActionRepository repository, Environment environment, ActionContext actionContext)
	{
		super(Action, repository)
		this.environment = environment
		this.actionContext = actionContext
	}

	@Override
	ResponseEntity save(@RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	ResponseEntity update(@PathVariable Long id, @RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	protected void bind(Action obj, Map<String, ?> payload)
	{
		throw new UnsupportedOperationException()
	}

	@PostMapping("/{id}/run")
	ResponseEntity run(@PathVariable Long id)
	{
		repository.findById(id).orElseThrow({ -> new RuntimeException() }).run(actionContext)
		return ResponseEntity.ok().build()
	}
}
