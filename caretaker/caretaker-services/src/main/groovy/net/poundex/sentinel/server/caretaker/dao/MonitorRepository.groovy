package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.ValueReader
import org.springframework.data.repository.PagingAndSortingRepository

interface MonitorRepository extends PagingAndSortingRepository<Monitor, Long>
{
	List<Monitor> findAllBySources(ValueReader valueReader)
}
