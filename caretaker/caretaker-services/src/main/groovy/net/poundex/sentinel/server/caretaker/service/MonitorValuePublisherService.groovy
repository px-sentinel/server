package net.poundex.sentinel.server.caretaker.service

import net.poundex.sentinel.server.caretaker.MonitorValuePublisher
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import org.springframework.integration.support.MessageBuilder
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Service

@Service
class MonitorValuePublisherService implements MonitorValuePublisher
{
	private final MessageChannel monitorValueChannel

	MonitorValuePublisherService(MessageChannel monitorValueChannel)
	{
		this.monitorValueChannel = monitorValueChannel
	}

	@Override
	void publish(Monitor monitor, MonitorValue monitorValue)
	{
		monitorValueChannel.send(MessageBuilder.withPayload(monitorValue).setHeader("Monitor", monitor).build())
	}
}
