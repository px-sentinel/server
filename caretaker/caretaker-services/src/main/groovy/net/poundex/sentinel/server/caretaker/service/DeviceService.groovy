package net.poundex.sentinel.server.caretaker.service

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.Driver
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Service

import java.time.Instant

@Service
@Slf4j
class DeviceService implements ApplicationListener<ContextRefreshedEvent>, DeviceManager
{
	private final List<Driver> drivers
	private final Map<String, Device> devices = [:]

	DeviceService(List<Driver> drivers)
	{
		this.drivers = drivers
	}

	@Override
	void onApplicationEvent(ContextRefreshedEvent event) {
		reset()
	}

	@Override
	void reset()
	{
		devices.clear()
		drivers.each { driver ->
			try {
				driver.start(this)
			} catch (Exception ex) {
				log.error("Driver failure", ex)
			}
		}
	}

	@Override
	Set<Device> getAllDevices()
	{
		devices.values().asImmutable().toSet()
	}

	@Override
	void setDeviceValue(URI deviceId, Object value)
	{
		String port = deviceId.getPath().substring(deviceId.getPath().lastIndexOf("/") + 1)
		String id = deviceId.resolve("self")
		Device device = devices[id]
		if( ! device) throw new RuntimeException()
		device.setValue(new DeviceValue(id, port, value, Instant.now()))
	}

	@Override
	void register(Device device)
	{
		devices[device.deviceId] = device
	}
}
