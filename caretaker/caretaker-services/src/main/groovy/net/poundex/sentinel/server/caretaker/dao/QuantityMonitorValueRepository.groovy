package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.monitor.value.QuantityMonitorValue
import org.springframework.data.repository.PagingAndSortingRepository

interface QuantityMonitorValueRepository extends PagingAndSortingRepository<QuantityMonitorValue, Long> { }
