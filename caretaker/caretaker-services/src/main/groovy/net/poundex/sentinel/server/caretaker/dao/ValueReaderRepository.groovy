package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.ValueReader
import org.springframework.data.repository.PagingAndSortingRepository

interface ValueReaderRepository extends PagingAndSortingRepository<ValueReader, Long>
{
	List<ValueReader> findAllByPortId(String source)
}
