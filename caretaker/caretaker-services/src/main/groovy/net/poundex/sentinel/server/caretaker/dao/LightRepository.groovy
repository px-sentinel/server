package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.appliance.Light
import org.springframework.data.repository.PagingAndSortingRepository

interface LightRepository extends PagingAndSortingRepository<Light, Long>
{
}
