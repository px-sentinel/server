package net.poundex.sentinel.server.caretaker.controller.monitor.value

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import org.springframework.data.repository.CrudRepository

abstract class AbstractMonitorValueController<T extends MonitorValue> extends ResourceController<T>
{
//	private final ZoneRepository zoneRepository

	AbstractMonitorValueController(Class<T> monitorClass, CrudRepository<T, Long> repository)
	{
		super(monitorClass, repository)
//		this.zoneRepository = zoneRepository
	}

	abstract void bindInternal(T obj, Map<String, ?> payload)

	@Override
	protected void bind(T obj, Map<String, ?> payload)
	{
		obj.name = payload.name
//		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
		bindInternal(obj, payload)
	}
}
