package net.poundex.sentinel.server.caretaker.controller.monitor

import net.poundex.sentinel.server.caretaker.controller.monitor.AbstractMonitorController
import net.poundex.sentinel.server.caretaker.dao.QuantityMonitorRepository
import net.poundex.sentinel.server.caretaker.dao.ZoneRepository
import net.poundex.sentinel.server.caretaker.domain.QuantityMonitor
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitor/quantity")
class QuantityMonitorController extends AbstractMonitorController<QuantityMonitor>
{
	QuantityMonitorController(QuantityMonitorRepository monitorRepository, ZoneRepository zoneRepository)
	{
		super(QuantityMonitor, monitorRepository, zoneRepository)
	}

	@Override
	void bindInternal(QuantityMonitor obj, Map<String, ?> payload)
	{
		obj.aggregator = QuantityMonitor.Aggregator.valueOf(payload.aggregator)
	}
}
