package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.monitor.AndPredicate
import org.springframework.data.repository.PagingAndSortingRepository

interface AndEnvironmentPredicateRepository extends PagingAndSortingRepository<AndPredicate, Long>
{

}
