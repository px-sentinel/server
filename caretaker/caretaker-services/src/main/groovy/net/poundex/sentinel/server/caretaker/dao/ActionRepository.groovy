package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.action.Action
import org.springframework.data.repository.PagingAndSortingRepository

interface ActionRepository extends PagingAndSortingRepository<Action, Long>
{

}
