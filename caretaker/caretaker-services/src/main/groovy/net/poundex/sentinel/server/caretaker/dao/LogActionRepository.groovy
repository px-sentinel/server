package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.action.LogAction
import org.springframework.data.repository.PagingAndSortingRepository

interface LogActionRepository extends PagingAndSortingRepository<LogAction, Long> { }
