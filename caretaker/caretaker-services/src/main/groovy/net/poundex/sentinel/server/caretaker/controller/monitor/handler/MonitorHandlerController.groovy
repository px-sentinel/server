package net.poundex.sentinel.server.caretaker.controller.monitor.handler

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.MonitorHandlerRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorHandler
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitorHandler")
class MonitorHandlerController extends ResourceController<MonitorHandler>
{
	MonitorHandlerController(MonitorHandlerRepository repository)
	{
		super(MonitorHandler, repository)
	}

	@Override
	protected void bind(MonitorHandler obj, Map<String, ?> payload) {
		obj.name = payload.name
//		obj.deviceId = payload.deviceId
//		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
	}
}
