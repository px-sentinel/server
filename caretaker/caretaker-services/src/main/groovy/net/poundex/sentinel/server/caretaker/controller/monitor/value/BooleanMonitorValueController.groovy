package net.poundex.sentinel.server.caretaker.controller.monitor.value

import net.poundex.sentinel.server.caretaker.controller.monitor.value.AbstractMonitorValueController
import net.poundex.sentinel.server.caretaker.dao.BooleanMonitorValueRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.value.BooleanMonitorValue
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/monitorValue/boolean")
class BooleanMonitorValueController extends AbstractMonitorValueController<BooleanMonitorValue>
{
	BooleanMonitorValueController(BooleanMonitorValueRepository repository)
	{
		super(BooleanMonitorValue, repository)
	}

	@Override
	void bindInternal(BooleanMonitorValue obj, Map<String, ?> payload)
	{
	}
}
