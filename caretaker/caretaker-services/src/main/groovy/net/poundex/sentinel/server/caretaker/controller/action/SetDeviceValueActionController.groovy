package net.poundex.sentinel.server.caretaker.controller.action


import net.poundex.sentinel.server.caretaker.dao.SetDeviceValueActionRepository
import net.poundex.sentinel.server.caretaker.domain.action.SetDeviceValueAction
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/action/setDeviceValue")
class SetDeviceValueActionController extends AbstractActionController<SetDeviceValueAction>
{
	SetDeviceValueActionController(SetDeviceValueActionRepository repository)
	{
		super(SetDeviceValueAction, repository)
	}

	@Override
	void bindInternal(SetDeviceValueAction obj, Map<String, ?> payload)
	{
//		obj.aggregator = BooleanMonitor.Aggregator.valueOf(payload.aggregator)
	}
}
