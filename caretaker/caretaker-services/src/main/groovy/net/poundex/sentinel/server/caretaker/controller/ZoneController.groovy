package net.poundex.sentinel.server.caretaker.controller

import net.poundex.sentinel.server.caretaker.dao.ZoneRepository
import net.poundex.sentinel.server.caretaker.domain.Zone
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/zone")
class ZoneController extends ResourceController<Zone>
{

	ZoneController(ZoneRepository zoneRepository)
	{
		super(Zone, zoneRepository)
	}

	@Override
	protected void bind(Zone obj, Map<String, ?> payload) {
		obj.name = payload.name
		if(payload.parent)
			obj.parent = repository.findById((Long)payload.parent?.id).orElseThrow({ new RuntimeException() })
	}
}
