package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorHandler
import org.springframework.data.repository.PagingAndSortingRepository

interface MonitorHandlerRepository extends PagingAndSortingRepository<MonitorHandler, Long>
{

}
