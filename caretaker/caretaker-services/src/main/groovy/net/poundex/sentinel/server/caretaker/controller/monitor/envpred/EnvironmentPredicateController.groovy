package net.poundex.sentinel.server.caretaker.controller.monitor.envpred

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.dao.EnvironmentPredicateRepository
import net.poundex.sentinel.server.caretaker.domain.monitor.EnvironmentPredicate
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/environmentPredicate")
class EnvironmentPredicateController extends ResourceController<EnvironmentPredicate>
{

	EnvironmentPredicateController(EnvironmentPredicateRepository repository)
	{
		super(EnvironmentPredicate, repository)
	}

	@Override
	ResponseEntity save(@RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	ResponseEntity update(@PathVariable Long id, @RequestBody Map<String, ?> payload)
	{
		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build()
	}

	@Override
	protected void bind(EnvironmentPredicate obj, Map<String, ?> payload)
	{
		throw new UnsupportedOperationException()
	}
}
