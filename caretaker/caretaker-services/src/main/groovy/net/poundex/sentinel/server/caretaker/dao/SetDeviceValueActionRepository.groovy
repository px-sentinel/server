package net.poundex.sentinel.server.caretaker.dao


import net.poundex.sentinel.server.caretaker.domain.action.SetDeviceValueAction
import org.springframework.data.repository.PagingAndSortingRepository

interface SetDeviceValueActionRepository extends PagingAndSortingRepository<SetDeviceValueAction, Long> { }
