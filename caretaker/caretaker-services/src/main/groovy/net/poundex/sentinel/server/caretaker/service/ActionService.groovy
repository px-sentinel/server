package net.poundex.sentinel.server.caretaker.service

import net.poundex.sentinel.server.caretaker.ActionContext
import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.MonitorValuePublisher
import net.poundex.sentinel.server.caretaker.domain.Monitor
import net.poundex.sentinel.server.caretaker.domain.appliance.Appliance
import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import net.poundex.sentinel.server.caretaker.enviornment.Environment
import org.springframework.stereotype.Service

@Service
class ActionService implements ActionContext
{
	private final DeviceManager deviceManager
	private final MonitorValuePublisher monitorValuePublisher
	final Environment environment

	ActionService(DeviceManager deviceManager, MonitorValuePublisher monitorValuePublisher, Environment environment)
	{
		this.deviceManager = deviceManager
		this.monitorValuePublisher = monitorValuePublisher
		this.environment = environment
	}

	@Override
	void setDeviceValue(Appliance appliance, String portId, Object value)
	{
		deviceManager.setDeviceValue(appliance.deviceId.toURI().resolve(portId), value)
	}

	@Override
	void setMonitorValue(Monitor monitor, MonitorValue monitorValue)
	{
		monitorValuePublisher.publish(monitor, monitorValue)
	}
}
