package net.poundex.sentinel.server.caretaker.controller

import net.poundex.sentinel.server.caretaker.dao.ApplianceRepository
import net.poundex.sentinel.server.caretaker.dao.ZoneRepository
import net.poundex.sentinel.server.caretaker.domain.appliance.Appliance
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/appliance")
class ApplianceController extends ResourceController<Appliance>
{
	private final ZoneRepository zoneRepository

	ApplianceController(ApplianceRepository repository, ZoneRepository zoneRepository)
	{
		super(Appliance, repository)
		this.zoneRepository = zoneRepository
	}

	@Override
	protected void bind(Appliance obj, Map<String, ?> payload) {
		obj.name = payload.name
		obj.deviceId = payload.deviceId
		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
	}
}
