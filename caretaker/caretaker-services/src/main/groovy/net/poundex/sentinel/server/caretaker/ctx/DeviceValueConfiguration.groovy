package net.poundex.sentinel.server.caretaker.ctx

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.service.EnvironmentService
import net.poundex.sentinel.server.caretaker.service.MonitorService
import net.poundex.sentinel.server.caretaker.service.ValueService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.config.EnableIntegration
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.MessageChannels
import org.springframework.messaging.MessageChannel

import java.util.concurrent.Executors

@Configuration
@EnableIntegration
@Slf4j
class DeviceValueConfiguration {
	@Bean
	MessageChannel deviceValueChannel() {
		return MessageChannels.publishSubscribe(Executors.newCachedThreadPool()).get()
	}

	@Bean
	IntegrationFlow deviceValueFlow(ValueService valueService) {
		return IntegrationFlows
				.from(deviceValueChannel())
				.handle(valueService)
				.get()
	}

	@Bean
	MessageChannel readValueChannel() {
		return MessageChannels.publishSubscribe(Executors.newCachedThreadPool()).get()
	}

	@Bean
	IntegrationFlow readValueFlow(MonitorService monitorService) {
		return IntegrationFlows
				.from(readValueChannel())
				.handle(monitorService)
				.get()
	}

	@Bean
	MessageChannel monitorValueChannel()
	{
		return MessageChannels.publishSubscribe(Executors.newCachedThreadPool()).get()
	}
	
	@Bean
	IntegrationFlow monitorValueFlow(MonitorService monitorService, EnvironmentService environmentService)
	{
		return IntegrationFlows
				.from(monitorValueChannel())
				.routeToRecipients({ r ->
					r.recipientFlow({ f -> f.handle(monitorService) })
					r.recipientFlow({ f -> f.handle(environmentService) })
				})
				.get()
	}


}
