package net.poundex.sentinel.server.caretaker.dao

import net.poundex.sentinel.server.caretaker.domain.monitor.MonitorValue
import org.springframework.data.repository.PagingAndSortingRepository

interface MonitorValueRepository extends PagingAndSortingRepository<MonitorValue, Long>
{
}
