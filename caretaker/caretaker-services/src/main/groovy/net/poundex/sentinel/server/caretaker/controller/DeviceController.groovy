package net.poundex.sentinel.server.caretaker.controller

import net.poundex.sentinel.server.caretaker.DeviceManager
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/device")
class DeviceController
{
	private final DeviceManager deviceManager

	DeviceController(DeviceManager deviceManager)
	{
		this.deviceManager = deviceManager
	}

	@GetMapping
	ResponseEntity index() {
		return ResponseEntity.ok(deviceManager.allDevices)
	}

	@PutMapping("/value")
	ResponseEntity setDeviceValue(@RequestBody Map request)
	{
		deviceManager.setDeviceValue(request.deviceId.toString().toURI(), request.value)
		return ResponseEntity.accepted().build()
	}
}
