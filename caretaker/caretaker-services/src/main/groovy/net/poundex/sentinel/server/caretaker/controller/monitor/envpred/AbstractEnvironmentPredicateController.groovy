package net.poundex.sentinel.server.caretaker.controller.monitor.envpred

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.domain.monitor.EnvironmentPredicate
import org.springframework.data.repository.CrudRepository

abstract class AbstractEnvironmentPredicateController<T extends EnvironmentPredicate> extends ResourceController<T>
{
	AbstractEnvironmentPredicateController(Class<T> monitorClass, CrudRepository<T, Long> repository)
	{
		super(monitorClass, repository)
	}

	abstract void bindInternal(T obj, Map<String, ?> payload)
	@Override
	protected void bind(T obj, Map<String, ?> payload)
	{
		obj.name = payload.name
//		obj.location = zoneRepository.findById((Long)payload.location?.id).orElseThrow({ new RuntimeException() })
		bindInternal(obj, payload)
	}
}
