package net.poundex.sentinel.server.caretaker.nest.controller

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat
import org.springframework.data.repository.CrudRepository
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/nest/thermostat")
class NestThermostatController extends ResourceController<NestThermostat>
{
	NestThermostatController(CrudRepository<NestThermostat, Long> repository)
	{
		super(NestThermostat, repository)
	}

	@Override
	protected void bind(NestThermostat obj, Map<String, ?> payload)
	{
		obj.name = payload.name
		obj.nestDeviceId = payload.nestDeviceId
		obj.nestAccessToken = payload.nestAccessToken
	}
}
