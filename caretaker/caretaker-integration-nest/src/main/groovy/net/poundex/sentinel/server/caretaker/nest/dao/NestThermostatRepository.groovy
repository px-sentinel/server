package net.poundex.sentinel.server.caretaker.nest.dao


import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat
import org.springframework.data.repository.PagingAndSortingRepository

interface NestThermostatRepository extends PagingAndSortingRepository<NestThermostat, Long>
{
}
