package net.poundex.sentinel.server.caretaker.nest.controller


import net.poundex.sentinel.server.caretaker.nest.service.NestDeviceService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/nest/device")
class NestDeviceController
{
	private final NestDeviceService nestDeviceService

	NestDeviceController(NestDeviceService nestDeviceService)
	{
		this.nestDeviceService = nestDeviceService
	}

	@GetMapping
	ResponseEntity index() {
		return ResponseEntity.ok(nestDeviceService.allDevices)
	}
}
