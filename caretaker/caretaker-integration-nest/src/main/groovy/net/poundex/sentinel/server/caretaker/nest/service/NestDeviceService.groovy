package net.poundex.sentinel.server.caretaker.nest.service

import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.DeviceValuePublisher
import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.Driver
import net.poundex.sentinel.server.caretaker.nest.dao.NestThermostatRepository
import net.poundex.sentinel.server.caretaker.nest.device.NestHeatingControllerDevice
import net.poundex.sentinel.server.caretaker.nest.device.NestHeatingThermostatDevice
import net.poundex.sentinel.server.caretaker.nest.device.NestReportingSensorDevice
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Service

@Service
class NestDeviceService implements Driver
{
	private final NestThermostatRepository nestThermostatRepository
	private final NestEventSourceService nestEventSourceService
	private final MessageChannel deviceValueChannel
	private final DeviceValuePublisher deviceValuePublisher

	private final Set<Device> registeredDevices = new HashSet<>()

	NestDeviceService(NestThermostatRepository nestThermostatRepository, NestEventSourceService nestEventSourceService, MessageChannel deviceValueChannel, DeviceValuePublisher deviceValuePublisher)
	{
		this.nestThermostatRepository = nestThermostatRepository
		this.nestEventSourceService = nestEventSourceService
		this.deviceValueChannel = deviceValueChannel
		this.deviceValuePublisher = deviceValuePublisher
	}

	@Override
	void start(DeviceManager deviceManager)
	{
		registeredDevices.clear()
		nestThermostatRepository.findAll().each { thermostat ->
			URI hardwareId = "nest://${thermostat.id}/self".toURI()
			NestHeatingControllerDevice controllerDevice =
					new NestHeatingControllerDevice(thermostat, "Nest Virtual Heating Controller", hardwareId.resolve("V_SWITCH/self").toString())
			NestHeatingThermostatDevice thermostatDevice =
					new NestHeatingThermostatDevice(thermostat, "Nest Thermostat", hardwareId.resolve("THERMOSTAT/self").toString())
			NestReportingSensorDevice sensorDevice =
					new NestReportingSensorDevice(thermostat, "Nest Reporting Sensor", hardwareId.resolve("SZ/self").toString(), deviceValuePublisher)

			nestEventSourceService.addEventTarget(thermostat, controllerDevice)
			nestEventSourceService.addEventTarget(thermostat, thermostatDevice)
			nestEventSourceService.addEventTarget(thermostat, sensorDevice)

			registeredDevices << controllerDevice
			registeredDevices << thermostatDevice
			registeredDevices << sensorDevice

			deviceManager.register(controllerDevice)
			deviceManager.register(thermostatDevice)
			deviceManager.register(sensorDevice)
		}
	}

	Set<Device> getAllDevices() {
		return registeredDevices.asImmutable()
	}
}
