package net.poundex.sentinel.server.caretaker.nest.clilent

import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.ToString

@ToString(includePackage = false, ignoreNulls = true)
class NestPayload
{
	String path

	@Delegate
	Data data

	@ToString(includePackage = false, ignoreNulls = true)
	static class Data
	{
		int humidity
		String locale
		@JsonProperty("temperature_scale")
		String temperatureScale
		@JsonProperty("ambient_temperature_c")
		double ambientTemperatureC
	}
}
