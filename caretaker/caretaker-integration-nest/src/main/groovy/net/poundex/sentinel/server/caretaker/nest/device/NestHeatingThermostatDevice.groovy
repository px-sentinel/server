package net.poundex.sentinel.server.caretaker.nest.device

import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort
import net.poundex.sentinel.server.caretaker.nest.clilent.NestEventTarget
import net.poundex.sentinel.server.caretaker.nest.clilent.NestPayload
import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat

@Slf4j
class NestHeatingThermostatDevice implements Device, NestEventTarget
{
	final NestThermostat hardware
	final String name
	final String deviceId

	NestHeatingThermostatDevice(NestThermostat hardware, String name, String deviceId)
	{
		this.hardware = hardware
		this.name = name
		this.deviceId = deviceId
	}

	@Override
	String getDeviceId()
	{
		return deviceId
	}

	@Override
	Set<ValuePort> getValuePorts()
	{
		return null
	}

	@Override
	void setValue(DeviceValue deviceValue)
	{

	}

	@Override
	void handleEvent(NestPayload nestPayload)
	{

	}
}
