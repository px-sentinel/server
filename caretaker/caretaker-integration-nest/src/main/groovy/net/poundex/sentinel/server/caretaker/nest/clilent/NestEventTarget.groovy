package net.poundex.sentinel.server.caretaker.nest.clilent

interface NestEventTarget
{
	void handleEvent(NestPayload nestPayload)
}
