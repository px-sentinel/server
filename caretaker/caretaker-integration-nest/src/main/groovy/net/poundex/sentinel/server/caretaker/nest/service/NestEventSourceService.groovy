package net.poundex.sentinel.server.caretaker.nest.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.launchdarkly.eventsource.EventHandler
import com.launchdarkly.eventsource.EventSource
import com.launchdarkly.eventsource.MessageEvent
import net.poundex.sentinel.server.caretaker.nest.clilent.NestEventTarget
import net.poundex.sentinel.server.caretaker.nest.clilent.NestPayload
import net.poundex.sentinel.server.caretaker.nest.dao.NestThermostatRepository
import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.springframework.stereotype.Service

@Service
class NestEventSourceService
{
	private final NestThermostatRepository nestThermostatRepository

	private final ObjectMapper objectMapper
	private final Map<NestThermostat, List<NestEventTarget>> managedDevices = [:]

	NestEventSourceService(ObjectMapper objectMapper, NestThermostatRepository nestThermostatRepository)
	{
		this.objectMapper = objectMapper
		this.nestThermostatRepository = nestThermostatRepository
	}

	void addEventTarget(NestThermostat thermostat, NestEventTarget eventTarget)
	{
		if( ! thermostat.serviceUrl)
			refreshServiceUrl(thermostat)

		if( ! managedDevices[thermostat])
			createClient(thermostat)

		managedDevices[thermostat] << eventTarget
	}

	void refreshServiceUrl(NestThermostat thermostat)
	{
		Response response = new OkHttpClient.Builder().followRedirects(false)
				.build().newCall(new Request.Builder()
				.url("https://developer-api.nest.com/devices/thermostats/${thermostat.nestDeviceId}")
				.addHeader("Authorization", "Bearer ${thermostat.nestAccessToken}")
				.addHeader("Accept", "application/json")
				.build()).execute()

		if(response.code() != 307)
			return

		thermostat.serviceUrl = response.header("Location")
		nestThermostatRepository.save(thermostat)
	}

	private void createClient(NestThermostat thermostat)
	{
		new EventSource.Builder(createEventHandler(thermostat),
				thermostat.serviceUrl.toURI())
				.headers(Headers.of([
					Authorization: "Bearer ${thermostat.nestAccessToken}".toString()]))
				.build()
				.start()
		managedDevices[thermostat] = []
	}

	private void handleClientEvent(NestThermostat thermostat, NestPayload payload)
	{
		managedDevices[thermostat].each {
			it.handleEvent(payload)
		}
	}

	private EventHandler createEventHandler(NestThermostat thermostat)
	{
		return new EventHandler() {
			@Override
			void onOpen() throws Exception { }

			@Override
			void onClosed() throws Exception { }

			@Override
			void onMessage(String event, MessageEvent messageEvent) throws Exception
			{
				if(event != "put")
					return
				handleClientEvent(thermostat,
					objectMapper.readValue(messageEvent.data, NestPayload))
			}

			@Override
			void onComment(String comment) throws Exception { }

			@Override
			void onError(Throwable t) { }
		}
	}
}
