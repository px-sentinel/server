package net.poundex.sentinel.server.caretaker.nest.device

import groovy.transform.CompileStatic
import groovy.transform.ToString
import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.DeviceValuePublisher
import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort
import net.poundex.sentinel.server.caretaker.nest.clilent.NestEventTarget
import net.poundex.sentinel.server.caretaker.nest.clilent.NestPayload
import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat

import java.time.Instant

@CompileStatic
@ToString(includePackage = false)
@Slf4j
class NestReportingSensorDevice implements NestEventTarget, Device
{
	static final String PORT_TEMPERATURE_C = "ROOM_TEMP_C"
	static final String PORT_TEMPERATURE_F = "ROOM_TEMP_F"
	static final String PORT_HUMIDITY = "HUMIDITY"
	static final Set<String> PORTS = [ PORT_TEMPERATURE_C, PORT_TEMPERATURE_F, PORT_HUMIDITY ].toSet().asImmutable()

	final NestThermostat hardware
	final String name
	final String deviceId

	private final DeviceValuePublisher deviceValuePublisher

	NestReportingSensorDevice(NestThermostat hardware, String name, String deviceId, DeviceValuePublisher deviceValuePublisher)
	{
		this.hardware = hardware
		this.name = name
		this.deviceId = deviceId
		this.deviceValuePublisher = deviceValuePublisher
	}

	@Override
	void handleEvent(NestPayload nestPayload)
	{
		Instant now = Instant.now()
		deviceValuePublisher.publish(
				new DeviceValue(
						deviceId,
						PORT_TEMPERATURE_C,
						nestPayload.data.ambientTemperatureC,
						now))

		deviceValuePublisher.publish(
				new DeviceValue(
						deviceId,
						PORT_HUMIDITY,
						nestPayload.data.humidity,
						now))
	}

	@Override
	Set<ValuePort> getValuePorts()
	{
		return null
	}

	@Override
	void setValue(DeviceValue deviceValue)
	{

	}
}
