package net.poundex.sentinel.server.caretaker.nest.device

import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort
import net.poundex.sentinel.server.caretaker.nest.clilent.NestEventTarget
import net.poundex.sentinel.server.caretaker.nest.clilent.NestPayload
import net.poundex.sentinel.server.caretaker.nest.domain.NestThermostat

class NestHeatingControllerDevice implements NestEventTarget, Device
{
	final NestThermostat hardware
	final String name
	final String deviceId

	NestHeatingControllerDevice(NestThermostat hardware, String name, String deviceId)
	{
		this.hardware = hardware
		this.name = name
		this.deviceId = deviceId
	}

	@Override
	void handleEvent(NestPayload nestPayload)
	{

	}

	@Override
	Set<ValuePort> getValuePorts()
	{
		return null
	}

	@Override
	void setValue(DeviceValue deviceValue)
	{

	}
}
