package net.poundex.sentinel.server.caretaker.nest.domain

import net.poundex.sentinel.server.caretaker.device.Hardware

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class NestThermostat implements Hardware
{
	@Id @GeneratedValue
	Long id

	String name

	String nestDeviceId
	String nestAccessToken
	String serviceUrl
}
