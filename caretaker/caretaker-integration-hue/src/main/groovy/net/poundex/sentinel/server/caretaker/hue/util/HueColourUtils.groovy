package net.poundex.sentinel.server.caretaker.hue.util


import net.poundex.sentinel.server.caretaker.hue.clilent.HueApiResponse.LightInfo.LightState
import java.awt.Color

class HueColourUtils
{
	static String getColourFromState(LightState state)
	{
		Color colour = Color.getHSBColor(state.hue / 65535, state.sat / 255, state.bri / 255)
		return "${colour.red},${colour.green},${colour.blue}"
	}

	static Map getStateFromColour(String colour)
	{
		String[] bits = colour.split(",")
		float[] fs = Color.RGBtoHSB(
				bits[0].toString().toDouble().multiply(255).toInteger(),
				bits[1].toString().toDouble().multiply(255).toInteger(),
				bits[2].toString().toDouble().multiply(255).toInteger(),
				null)
		return [hue: (fs[0] * 65535).toInteger(), sat: (fs[1] * 255).toInteger(), bri: (fs[2] * 255).toInteger()]
	}
}
