package net.poundex.sentinel.server.caretaker.hue.clilent

import feign.Param
import feign.RequestLine

interface HueBridgeClient
{
	@RequestLine("GET /")
	HueApiResponse getEverything()

	@RequestLine("PUT /lights/{bulbId}/state")
	List<Map> setLightState(
			@Param("bulbId") String bulbId,
			Map state)
}
