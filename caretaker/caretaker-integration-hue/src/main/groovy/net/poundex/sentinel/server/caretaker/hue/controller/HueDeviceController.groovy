package net.poundex.sentinel.server.caretaker.hue.controller

import net.poundex.sentinel.server.caretaker.hue.service.HueDeviceService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hue/device")
class HueDeviceController
{
	private final HueDeviceService hueDeviceService

	HueDeviceController(HueDeviceService hueDeviceService)
	{
		this.hueDeviceService = hueDeviceService
	}

	@GetMapping
	ResponseEntity index() {
		return ResponseEntity.ok(hueDeviceService.allDevices)
	}
}
