package net.poundex.sentinel.server.caretaker.hue.service

import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.device.Driver
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ReadWriteValuePort
import net.poundex.sentinel.server.caretaker.hue.clilent.HueApiResponse
import net.poundex.sentinel.server.caretaker.hue.clilent.HueBridgeClient
import net.poundex.sentinel.server.caretaker.hue.clilent.HueBridgeClientFactory
import net.poundex.sentinel.server.caretaker.hue.dao.HueBridgeRepository
import net.poundex.sentinel.server.caretaker.hue.device.HueBulbDevice
import net.poundex.sentinel.server.caretaker.hue.domain.HueBridge
import net.poundex.sentinel.server.caretaker.hue.util.HueColourUtils
import org.springframework.stereotype.Service

import java.time.Instant

@Service
class HueDeviceService implements Driver
{
	private final HueBridgeRepository hueBridgeRepository
	private final HueBridgeClientFactory hueBridgeClientFactory

	private final Set<HueBulbDevice> registeredDevices = new HashSet<>()

	HueDeviceService(HueBridgeRepository hueBridgeRepository, HueBridgeClientFactory hueBridgeClientFactory)
	{
		this.hueBridgeRepository = hueBridgeRepository
		this.hueBridgeClientFactory = hueBridgeClientFactory
	}

	@Override
	void start(DeviceManager deviceManager)
	{
		registeredDevices.clear()
		hueBridgeRepository.findAll().each { bridge ->
			HueBridgeClient client = hueBridgeClientFactory
					.getClientForUrl("http://${bridge.address}/api/${bridge.username}")
			client.everything.lights.collect { bulbId, info ->
				HueBulbDevice hbd = configureBulbDevice(bridge, bulbId, info, client)
				registeredDevices << hbd
				deviceManager.register(hbd)
			}
		}
	}

	private HueBulbDevice configureBulbDevice(HueBridge bridge, String bulbId, HueApiResponse.LightInfo info, HueBridgeClient hueBridgeClient)
	{
		HueBulbDevice hbd = new HueBulbDevice("hue://${bridge.id}/bulb/${bulbId}/self", bridge,
				info.name, info.type, info.modelid, info.manufacturername,
				info.productname, info.uniqueid, bulbId)
		configureBulbPorts(hbd, hueBridgeClient)
		return hbd
	}

	private void configureBulbPorts(HueBulbDevice hueBulbDevice, HueBridgeClient hueBridgeClient)
	{
		ReadWriteValuePort power = new ReadWriteValuePort() {
			@Override
			DeviceValue getValue()
			{
				return new DeviceValue(hueBulbDevice.deviceId, "POWER",
						hueBridgeClient.getEverything().lights[hueBulbDevice.bulbId].state.on, Instant.now())
			}

			@Override
			void setValue(DeviceValue value)
			{
				hueBridgeClient.setLightState(hueBulbDevice.bulbId, [on: value.value])
			}

			@Override
			String getId()
			{
				return "POWER"
			}

			@Override
			String getType()
			{
				return "bool"
			}
		}

		ReadWriteValuePort colour = new ReadWriteValuePort() {
			@Override
			DeviceValue getValue()
			{
				return new DeviceValue(hueBulbDevice.deviceId, "COLOUR",
						HueColourUtils.getColourFromState(hueBridgeClient.getEverything().lights[hueBulbDevice.bulbId].state), Instant.now())
			}

			@Override
			void setValue(DeviceValue value)
			{
				String raw = value.value
				String[] bits = raw.split("/")
				Map m = [on: true]
				if(bits[0] != "_")
					m += HueColourUtils.getStateFromColour(bits[0])
				if(bits.size() > 1)
					m += [bri: ((bits[1].toInteger() / 100) * 254).intValue()]
				hueBridgeClient.setLightState(hueBulbDevice.bulbId, m)
			}

			@Override
			String getId()
			{
				return "COLOUR"
			}

			@Override
			String getType()
			{
				return "colour"
			}
		}

		ReadWriteValuePort ctemp = new ReadWriteValuePort() {
			@Override
			DeviceValue getValue()
			{
				return new DeviceValue(hueBulbDevice.deviceId, "CT", hueBridgeClient.getEverything().lights[hueBulbDevice.bulbId].state.ct, Instant.now())
			}

			@Override
			void setValue(DeviceValue value)
			{
				String raw = value.value
				String[] bits = raw.split("/")
				Map m = [on: true]
				if(bits[0] != "_")
					m += [ct: bits[0].toInteger()]
				if(bits.size() > 1)
					m += [bri: ((bits[1].toInteger() / 100) * 254).intValue()]
				hueBridgeClient.setLightState(hueBulbDevice.bulbId, m)
			}

			@Override
			String getId()
			{
				return "CT"
			}

			@Override
			String getType()
			{
				return "ct"
			}
		}
		hueBulbDevice.setValuePorts([power, colour, ctemp].toSet())
	}

	Set<HueBulbDevice> getAllDevices() {
		return registeredDevices.asImmutable()
	}
}
