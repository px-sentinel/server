package net.poundex.sentinel.server.caretaker.hue.device


import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort
import net.poundex.sentinel.server.caretaker.device.value.WritableValuePort
import net.poundex.sentinel.server.caretaker.hue.domain.HueBridge

class HueBulbDevice implements Device
{
	final String deviceId
	final HueBridge hardware

	final String name
	final String type
	final String modelId
	final String manufacturer
	final String productName
	final String uniqueId
	final String bulbId

	private final Set<ValuePort> valuePorts = new HashSet<>()

	HueBulbDevice(String deviceId, HueBridge hardware, String name, String type, String modelId, String manufacturer, String productName, String uniqueId, String bulbId)
	{
		this.deviceId = deviceId
		this.hardware = hardware
		this.name = name
		this.type = type
		this.modelId = modelId
		this.manufacturer = manufacturer
		this.productName = productName
		this.uniqueId = uniqueId
		this.bulbId = bulbId
	}

	@Override
	Set<ValuePort> getValuePorts()
	{
		return valuePorts
	}

	@Override
	void setValue(DeviceValue deviceValue)
	{
		ValuePort valuePort = valuePorts.find { it.id == deviceValue.portName }
		if(valuePort instanceof WritableValuePort)
			valuePort.setValue(deviceValue)
	}

	void setValuePorts(Set<ValuePort> ports)
	{
		valuePorts.clear()
		valuePorts.addAll(ports)
	}
}
