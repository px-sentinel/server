package net.poundex.sentinel.server.caretaker.hue.domain

import net.poundex.sentinel.server.caretaker.device.Hardware

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class HueBridge implements Hardware
{
	@Id @GeneratedValue
	Long id

	String name
	String address
	String username

	URI getHardwareId() {
		return "hue://${id}/self".toURI()
	}
}
