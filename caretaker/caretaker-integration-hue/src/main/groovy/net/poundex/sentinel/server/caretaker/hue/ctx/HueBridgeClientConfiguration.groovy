package net.poundex.sentinel.server.caretaker.hue.ctx

import feign.Feign
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import net.poundex.sentinel.server.caretaker.hue.clilent.HueBridgeClient
import net.poundex.sentinel.server.caretaker.hue.clilent.HueBridgeClientFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import java.util.function.Supplier

@Configuration
class HueBridgeClientConfiguration
{
	@Bean
	HueBridgeClientFactory hueBridgeClient(){
		return { String url ->
			Feign.builder()
					.encoder(new JacksonEncoder())
					.decoder(new JacksonDecoder())
					.target(HueBridgeClient, url)
		}
	}
}
