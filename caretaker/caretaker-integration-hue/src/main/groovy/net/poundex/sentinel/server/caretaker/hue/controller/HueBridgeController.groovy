package net.poundex.sentinel.server.caretaker.hue.controller

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.hue.domain.HueBridge
import org.springframework.data.repository.CrudRepository
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hue/bridge")
class HueBridgeController extends ResourceController<HueBridge>
{
	HueBridgeController(CrudRepository<HueBridge, Long> repository)
	{
		super(HueBridge, repository)
	}

	@Override
	protected void bind(HueBridge obj, Map<String, ?> payload)
	{
		obj.name = payload.name
		obj.address = payload.address
		obj.username = payload.username
	}
}
