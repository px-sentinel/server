package net.poundex.sentinel.server.caretaker.hue.dao

import net.poundex.sentinel.server.caretaker.hue.domain.HueBridge
import org.springframework.data.repository.PagingAndSortingRepository

interface HueBridgeRepository extends PagingAndSortingRepository<HueBridge, Long>
{
}
