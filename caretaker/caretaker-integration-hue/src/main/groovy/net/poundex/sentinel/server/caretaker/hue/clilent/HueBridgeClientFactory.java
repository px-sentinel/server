package net.poundex.sentinel.server.caretaker.hue.clilent;

import java.util.function.Function;

public interface HueBridgeClientFactory extends Function<String, HueBridgeClient>
{
	default HueBridgeClient getClientForUrl(String url) {
		return this.apply(url);
	}
}
