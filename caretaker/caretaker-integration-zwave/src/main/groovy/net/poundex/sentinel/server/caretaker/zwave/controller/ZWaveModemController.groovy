package net.poundex.sentinel.server.caretaker.zwave.controller

import net.poundex.sentinel.server.caretaker.controller.ResourceController
import net.poundex.sentinel.server.caretaker.zwave.dao.ZWaveModemRepository
import net.poundex.sentinel.server.caretaker.zwave.domain.ZWaveModem
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/zwave/modem")
class ZWaveModemController extends ResourceController<ZWaveModem>
{
	ZWaveModemController(ZWaveModemRepository repository)
	{
		super(ZWaveModem, repository)
	}

	@Override
	protected void bind(ZWaveModem obj, Map<String, ?> payload)
	{
		obj.name = payload.name
		obj.modemDevice = payload.modemDevice
	}
}
