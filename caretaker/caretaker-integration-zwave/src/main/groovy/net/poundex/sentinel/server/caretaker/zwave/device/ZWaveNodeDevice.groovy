package net.poundex.sentinel.server.caretaker.zwave.device

import com.whizzosoftware.wzwave.commandclass.AlarmCommandClass
import com.whizzosoftware.wzwave.commandclass.BatteryCommandClass
import com.whizzosoftware.wzwave.commandclass.BinarySensorCommandClass
import com.whizzosoftware.wzwave.commandclass.CentralSceneCommandClass
import com.whizzosoftware.wzwave.commandclass.MultilevelSensorCommandClass
import com.whizzosoftware.wzwave.node.ZWaveEndpoint
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.DeviceValuePublisher
import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.Hardware
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort
import net.poundex.sentinel.server.caretaker.domain.monitor.value.ButtonPushMonitorValue
import net.poundex.sentinel.server.caretaker.event.AlarmEvent
import net.poundex.sentinel.server.caretaker.event.ButtonPushEvent

import java.time.Instant

@Slf4j
@CompileStatic
class ZWaveNodeDevice implements Device
{
	final String deviceId
	final Hardware hardware
	final String name

	private final ZWaveEndpoint node
	private final DevicePublisherProxy deviceValuePublisher

	private Integer lastSeenSequenceNumber

	ZWaveNodeDevice(String deviceId, Hardware hardware, String name, ZWaveEndpoint node, DeviceValuePublisher deviceValuePublisher)
	{
		this.deviceId = deviceId
		this.hardware = hardware
		this.name = name
		this.node = node
		this.deviceValuePublisher = new DevicePublisherProxy(deviceValuePublisher)
	}

	@Override
	Set<ValuePort> getValuePorts()
	{
		return null
	}

	@Override
	void setValue(DeviceValue deviceValue)
	{

	}

	void update()
	{
		node.commandClasses.each { cc ->
			if(cc instanceof MultilevelSensorCommandClass)
				updateMultilevel(cc)
			else if(cc instanceof BinarySensorCommandClass)
				updateBinarySensor(cc)
			else if(cc instanceof CentralSceneCommandClass)
				handleSceneCommand(cc)
			else if(cc instanceof AlarmCommandClass)
				handleAlarmStatus(cc)
			else if(cc instanceof BatteryCommandClass)
				handleBatteryStatus(cc)
		}
	}

	private void updateMultilevel(MultilevelSensorCommandClass cc)
	{
		String portName = getPortForMultilevelType(cc.type)
		deviceValuePublisher.publish(new DeviceValue(deviceId, portName, cc.values?.first(), Instant.now()))
	}

	private void updateBinarySensor(BinarySensorCommandClass cc)
	{
		deviceValuePublisher.publish(new DeviceValue(deviceId, "BINARY_SZ", ! cc.isIdle, Instant.now()))
	}

	private void handleSceneCommand(CentralSceneCommandClass cc)
	{
		if(cc.sequenceNumber == lastSeenSequenceNumber)
			return

		lastSeenSequenceNumber = cc.sequenceNumber
		deviceValuePublisher.publishAlways(new DeviceValue(deviceId, "SCENE_COMMAND", new ButtonPushEvent(
				getTypeForSceneCommand(cc.sceneCommand), cc.pushCount, cc.sceneNumber), Instant.now()))
	}

	static ButtonPushMonitorValue.Type getTypeForSceneCommand(CentralSceneCommandClass.SceneCommand cmd)
	{
		switch (cmd) {
			case CentralSceneCommandClass.SceneCommand.PUSHED:
				return ButtonPushMonitorValue.Type.PUSHED
			case CentralSceneCommandClass.SceneCommand.RELEASED_AFTER_HOLD:
				return ButtonPushMonitorValue.Type.RELEASED_AFTER_HOLD
			case CentralSceneCommandClass.SceneCommand.BEING_HELD:
				return ButtonPushMonitorValue.Type.BEING_HELD_DOWN
		}
	}

	static String getPortForMultilevelType(MultilevelSensorCommandClass.Type type)
	{
		switch (type) {
			case MultilevelSensorCommandClass.Type.AirTemperature:
				return "AIR_TEMPERATURE"
			case MultilevelSensorCommandClass.Type.Humidity:
				return "HUMIDITY"
			default:
				return "UNKNOWN"
		}
	}

	void handleAlarmStatus(AlarmCommandClass cc)
	{
		deviceValuePublisher.publish(new DeviceValue(deviceId, "ALARM", new AlarmEvent(
				cc.type, cc.status, cc.actualType, cc.status), Instant.now()))
	}

	void handleBatteryStatus(BatteryCommandClass cc)
	{
		// TODO
//		log.error("Not handling battery info like I should be {}: {}%", deviceId, cc.level)
	}

	private static class DevicePublisherProxy implements DeviceValuePublisher {
		private final DeviceValuePublisher delegate
		private final Map<String, DeviceValue> lastKnownValue = [:]

		DevicePublisherProxy(DeviceValuePublisher delegate)
		{
			this.delegate = delegate
		}

		@Override
		void publish(DeviceValue deviceValue)
		{
			DeviceValue lastKnown = lastKnownValue[deviceValue.portId]
			if(lastKnown && lastKnown.value == deviceValue.value)
				return
			publishAlways(deviceValue)
		}

		void publishAlways(DeviceValue deviceValue)
		{
			delegate.publish(deviceValue)
			lastKnownValue[deviceValue.portId] = deviceValue
		}
	}
}
