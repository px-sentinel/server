package net.poundex.sentinel.server.caretaker.zwave


import com.whizzosoftware.wzwave.controller.ZWaveController
import com.whizzosoftware.wzwave.controller.ZWaveControllerListener
import com.whizzosoftware.wzwave.controller.netty.NettyZWaveController
import com.whizzosoftware.wzwave.node.NodeInfo
import com.whizzosoftware.wzwave.node.ZWaveEndpoint
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.device.Driver
import net.poundex.sentinel.server.caretaker.zwave.dao.ZWaveModemRepository
import net.poundex.sentinel.server.caretaker.zwave.device.ZWaveNodeDevice
import net.poundex.sentinel.server.caretaker.zwave.domain.ZWaveModem
import net.poundex.sentinel.server.caretaker.zwave.service.ZWaveDeviceFactory
import org.springframework.stereotype.Service

import java.nio.file.Files
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Service
@Slf4j
@CompileStatic
class ZWaveDeviceService implements Driver
{
	private final ZWaveModemRepository zWaveModemRepository
	private final ZWaveDeviceFactory zWaveDeviceFactory

	private final Map<ZWaveModem, ZWaveController> modemControllers = [:]
	private final Map<ZWaveEndpoint, ZWaveNodeDevice> devices = [:]
	private final Map<ZWaveModem, Set<Byte>> knownNodes = [:]

	private final ExecutorService executorService = Executors.newCachedThreadPool(this.&executorThread)

	ZWaveDeviceService(ZWaveModemRepository zWaveModemRepository, ZWaveDeviceFactory zWaveDeviceFactory)
	{
		this.zWaveModemRepository = zWaveModemRepository
		this.zWaveDeviceFactory = zWaveDeviceFactory
	}

	@Override
	void start(DeviceManager deviceManager)
	{
		zWaveModemRepository.findAll().each { modem ->
			knownNodes[modem] = new HashSet<Byte>()
			ZWaveController controller = new NettyZWaveController(modem.modemDevice, Files.createTempDirectory("zwavetmp").toFile())
			controller.listener = new Listener(deviceManager, modem)
			modemControllers[modem] = controller
			controller.start()
		}
	}

	private void registerNodeDevice(ZWaveEndpoint node, DeviceManager deviceManager, ZWaveModem modem)
	{
		if(knownNodes[modem].contains(node.nodeId)) return

		ZWaveNodeDevice device = zWaveDeviceFactory.createDevice(node, modem)
		deviceManager.register(device)
		log.info("Registered ZWave node device {} ({}@{})", device.name, node.nodeId, modem.name)
		devices[node] = device

		knownNodes[modem] << node.nodeId
	}

	private void handleNodeUpdate(ZWaveEndpoint node)
	{
		devices[node].update()
	}

	@CompileStatic
	private class Listener implements ZWaveControllerListener
	{
		private final DeviceManager deviceManager
		private final ZWaveModem modem

		Listener(DeviceManager deviceManager, ZWaveModem modem)
		{
			this.deviceManager = deviceManager
			this.modem = modem
		}

		@Override
		void onZWaveNodeAdded(ZWaveEndpoint node)
		{
			doLater {
				registerNodeDevice(node, deviceManager, modem)
				handleNodeUpdate(node)
			}
		}

		@Override
		void onZWaveNodeUpdated(ZWaveEndpoint node)
		{
			doLater {
				registerNodeDevice(node, deviceManager, modem)
				handleNodeUpdate(node)
			}
		}

		@Override
		void onZWaveConnectionFailure(Throwable t) {
			log.error("ZWave Modem Connection Failure", t)
		}

		@Override
		void onZWaveControllerInfo(String libraryVersion, Integer homeId, Byte nodeId) {
			log.warn "controller info"
		}

		@Override
		void onZWaveInclusionStarted() {
			log.warn "inclusion started"
		}

		@Override
		void onZWaveInclusion(NodeInfo nodeInfo, boolean success) {
			log.warn "inclusion"
		}

		@Override
		void onZWaveInclusionStopped() {
			log.warn "inclusion stopped"
		}

		@Override
		void onZWaveExclusionStarted() {
			log.warn "exclusion started"
		}

		@Override
		void onZWaveExclusion(NodeInfo nodeInfo, boolean success) {
			log.warn "exclusion"
		}

		@Override
		void onZWaveExclusionStopped() {
			log.warn "exclusion stopped"
		}

		private void doLater(Runnable r) {
			executorService.submit(r)
		}
	}

	private static Thread executorThread(Runnable r)
	{
		Thread t = new Thread(r)
		t.daemon = true
		t.uncaughtExceptionHandler = { Thread thread, Throwable ex ->
			log.error("Bad on ZWave executor thread", ex)
		}
		return t
	}
}
