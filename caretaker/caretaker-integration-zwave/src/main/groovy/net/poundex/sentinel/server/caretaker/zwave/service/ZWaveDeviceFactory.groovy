package net.poundex.sentinel.server.caretaker.zwave.service

import com.whizzosoftware.wzwave.commandclass.ManufacturerSpecificCommandClass
import com.whizzosoftware.wzwave.node.ZWaveEndpoint
import net.poundex.sentinel.server.caretaker.DeviceValuePublisher
import net.poundex.sentinel.server.caretaker.zwave.device.ZWaveNodeDevice
import net.poundex.sentinel.server.caretaker.zwave.domain.ZWaveModem
import org.springframework.stereotype.Service

@Service
class ZWaveDeviceFactory
{
	private final DeviceValuePublisher deviceValuePublisher

	ZWaveDeviceFactory(DeviceValuePublisher deviceValuePublisher)
	{
		this.deviceValuePublisher = deviceValuePublisher
	}

	ZWaveNodeDevice createDevice(ZWaveEndpoint node, ZWaveModem modem)
	{
		String deviceId = "zwave://${modem.id}/node/${node.nodeId}/self"
		String makeAndModel = getMakeAndModelString(node) ?: "Unknown"
		ZWaveNodeDevice device =
				new ZWaveNodeDevice(deviceId, modem, "${node.class.simpleName} ZWave Device (${makeAndModel})", node, deviceValuePublisher)

		return device
	}

	static String getMakeAndModelString(ZWaveEndpoint node)
	{
		return node.getCommandClass(ManufacturerSpecificCommandClass.ID)?.with { ManufacturerSpecificCommandClass cc ->
			"${cc.productInfo?.manufacturer} ${cc.productInfo?.name}"
		}
	}
}
