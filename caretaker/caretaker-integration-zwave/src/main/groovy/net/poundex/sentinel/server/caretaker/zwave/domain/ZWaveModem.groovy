package net.poundex.sentinel.server.caretaker.zwave.domain

import net.poundex.sentinel.server.caretaker.device.Hardware

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class ZWaveModem implements Hardware
{
	@Id @GeneratedValue
	Long id

	String name
	String modemDevice
}
