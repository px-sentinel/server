package net.poundex.sentinel.server.caretaker.zwave.dao

import net.poundex.sentinel.server.caretaker.zwave.domain.ZWaveModem
import org.springframework.data.repository.PagingAndSortingRepository

interface ZWaveModemRepository extends PagingAndSortingRepository<ZWaveModem, Long> { }
