package net.poundex.sentinel.server.caretaker.tools.daylight

import net.poundex.sentinel.server.caretaker.DeviceManager
import net.poundex.sentinel.server.caretaker.DeviceValuePublisher
import net.poundex.sentinel.server.caretaker.device.Driver
import net.poundex.sentinel.server.caretaker.device.Hardware
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import org.springframework.stereotype.Service

import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId

@Service
class FakeDaylightDriverService implements Driver
{
	private final DeviceValuePublisher deviceValuePublisher

	private final FakeDaylightDevice device

	private final LocalTime on = LocalTime.of(9, 0)
	private final LocalTime off = LocalTime.of(19, 0)

	private Boolean lastValueSent

	FakeDaylightDriverService(DeviceValuePublisher deviceValuePublisher)
	{
		this.device = new FakeDaylightDevice("dummy://daylight/self", FakeDaylightHardware.instance, "Fake Daylight Provider")
		this.deviceValuePublisher = deviceValuePublisher
	}

	@Override
	void start(DeviceManager deviceManager)
	{
		deviceManager.register(device)
	}

	@Singleton
	private static class FakeDaylightHardware implements Hardware {

	}

	void tick()
	{
		Instant now = Instant.now()
		value = now.isAfter(on.atDate(LocalDate.now()).atZone(ZoneId.of("Europe/London")).toInstant()) &&
				now.isBefore(off.atDate(LocalDate.now()).atZone(ZoneId.of("Europe/London")).toInstant())
	}

	private void setValue(boolean isDaylight)
	{
		if(isDaylight == lastValueSent)
			return

		deviceValuePublisher.publish(new DeviceValue(device.deviceId, "IS_DAYLIGHT", isDaylight, Instant.now()))
		lastValueSent = isDaylight
	}
}
