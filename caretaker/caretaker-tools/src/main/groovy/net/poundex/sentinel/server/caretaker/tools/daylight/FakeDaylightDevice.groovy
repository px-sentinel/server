package net.poundex.sentinel.server.caretaker.tools.daylight

import net.poundex.sentinel.server.caretaker.device.Device
import net.poundex.sentinel.server.caretaker.device.Hardware
import net.poundex.sentinel.server.caretaker.device.value.DeviceValue
import net.poundex.sentinel.server.caretaker.device.value.ValuePort

class FakeDaylightDevice implements Device
{
	final String deviceId
	final Hardware hardware
	final String name

	FakeDaylightDevice(String deviceId, Hardware hardware, String name)
	{
		this.deviceId = deviceId
		this.hardware = hardware
		this.name = name
	}

	@Override
	Set<ValuePort> getValuePorts()
	{
		return null
	}

	@Override
	void setValue(DeviceValue deviceValue)
	{

	}
}
