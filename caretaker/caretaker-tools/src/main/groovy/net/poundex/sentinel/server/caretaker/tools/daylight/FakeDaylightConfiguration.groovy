package net.poundex.sentinel.server.caretaker.tools.daylight

import org.quartz.JobDetail
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.quartz.CronTriggerFactoryBean
import org.springframework.scheduling.quartz.JobDetailFactoryBean

@Configuration
class FakeDaylightConfiguration
{
	@Bean
	JobDetailFactoryBean jobDetail() {
		JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean()
		jobDetailFactory.jobClass = FakeDaylightJob.class
		jobDetailFactory.description = "Fake daylight job"
		jobDetailFactory.durability = true
		return jobDetailFactory
	}

	@Bean
	CronTriggerFactoryBean trigger(JobDetail job) {
		CronTriggerFactoryBean trigger = new CronTriggerFactoryBean()
		trigger.jobDetail = job
		trigger.cronExpression = "0 0/1 * ? * * *"
		return trigger
	}
}
