package net.poundex.sentinel.server.caretaker.tools.daylight

import groovy.util.logging.Slf4j
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException

@Slf4j
class FakeDaylightJob implements Job
{
	private final FakeDaylightDriverService fakeDaylightDriverService

	FakeDaylightJob(FakeDaylightDriverService fakeDaylightDriverService)
	{
		this.fakeDaylightDriverService = fakeDaylightDriverService
	}

	@Override
	void execute(JobExecutionContext context) throws JobExecutionException
	{
		fakeDaylightDriverService.tick()
	}
}
